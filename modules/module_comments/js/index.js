$(document).ready(function(){
    var slider = $('.comments_slider_block');
    $(slider).owlCarousel({
        items:1,
        itemsCustom : false,
        autoPlay : true,
        lazyLoad: true,
        singleItem:true
    });
    $(".button_left").click(function () {
        slider.trigger('owl.next');
    });
    $(".button_right").click(function () {
        slider.trigger('owl.prev');
    });
});
