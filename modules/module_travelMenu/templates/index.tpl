<div class="module_travelMenu">
    {foreach from=$travelMenu item=travelMenuItem}
        {if $tickets == {$travelMenuItem.search}}
            {assign var=menuClass value='travel_menu tooltipped active_travelMenu'}
        {else}
            {assign var=menuClass value='travel_menu tooltipped'}
        {/if}
        <a href="{$travelMenuItem.seoLink}"
           data-position="top"
           data-tooltip="{$travelMenuItem.title}"
           id="{$travelMenuItem.search}"
           class="{$menuClass}">
            <span class="travel_menu_title">
                <i class="fa {$travelMenuItem.icon}"></i>
                <i class="travel_title">{$travelMenuItem.title}</i>
            </span>
        </a>
    {/foreach}
</div>