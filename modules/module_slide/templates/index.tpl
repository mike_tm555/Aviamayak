<div class="module_slide">
    <div class="slider fullscreen">
        <span class="slide-button slide-prev">
            <i class="fa fa-angle-left"></i>
        </span>
        <span class="slide-button slide-next">
            <i class="fa fa-angle-right"></i>
        </span>
        <div class="slider-top">
        </div>
        <ul class="slides">
            {foreach from=$slider.slider item=sliderItem}
                <li>
                    <img src="{$sliderItem.image}">
                    <div class="{$sliderItem.captionClass}">
                        <h3>{$sliderItem.title}</h3>
                        <h5 class="light grey-text text-lighten-3">{$sliderItem.subTitle}</h5>
                    </div>
                </li>
            {/foreach}
        </ul>
        <div class="slider-bottom">
        </div>
    </div>
</div>