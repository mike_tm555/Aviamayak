<div class="module_siteMap">
    <div class="module_siteMap_inner">
    {foreach from=$siteMap item=mapItem}
        <div class="siteMap_block">
            <span class="siteMap_title">{$mapItem.title}</span>
            {foreach from=$mapItem.items item=multiItems}
            <a href="{$multiItems.seoLink}" class="siteMap_item">{$multiItems.title}</a>
            {/foreach}
        </div>
    {/foreach}
</div>
</div>