<?php
class module_footer extends  module
{
    protected function getData()
    {
        $this->data['footer'] = $this->getModuleSettings();
        $this->data['contactUsForm'] = plugin_forms::getInstance()->createForm('contactUsForm');
    }
}