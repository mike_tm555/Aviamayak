<div class="module_footer">
    <footer class="page-footer">
        <div class="container footer-header">
            <div class="row">
                <div class="col l6 s12 info requisites-block">
                    <h5 class="white-text">
                        {$footer.requisites.title}
                    </h5>
                    <p class="grey-text text-lighten-4">
                        {$footer.requisites.text}
                    </p>
                    <ul class="requisites_list">
                        {foreach from=$footer.requisites.items item=requisite}
                            <li>
                                <a class="waves-effect waves-light btn-flat requisite">
                                    <i class="{$requisite.icon}"></i>
                                    &nbsp;{$requisite.title}
                                </a>
                            </li>
                        {/foreach}
                    </ul>
                </div>
                <div class="col l6 offset-l2 s12 form-block">
                    <h5 class="white-text">
                        {$footer.formTitle}
                    </h5>
                    <div class="footer-form">
                    {$contactUsForm}
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                {$footer.copyRight}
                <a class="site-map text-lighten-4 waves-effect waves-light right" href="{$footer.siteMap.soLink}">
                    <i class="{$footer.siteMap.icon}"></i>
                    {$footer.siteMap.title}
                </a>
            </div>
        </div>
    </footer>
</div>