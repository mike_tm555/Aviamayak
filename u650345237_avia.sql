
-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 24, 2015 at 07:05 AM
-- Server version: 10.0.20-MariaDB
-- PHP Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `u650345237_avia`
--

-- --------------------------------------------------------

--
-- Table structure for table `Contents`
--

CREATE TABLE IF NOT EXISTS `Contents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` tinyint(4) NOT NULL,
  `class` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `Contents`
--

INSERT INTO `Contents` (`id`, `page_id`, `class`, `content`) VALUES
(1, 2, 'large', 'hjggfsdjd sjfgsdgfhgd');

-- --------------------------------------------------------

--
-- Table structure for table `Destinations`
--

CREATE TABLE IF NOT EXISTS `Destinations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `Destinations`
--

INSERT INTO `Destinations` (`id`, `title`, `text`, `data`) VALUES
(1, 'Jamaica', '', '{"image":"/images/destination1.jpg"}'),
(2, 'New York City ', '', '{"image":"/images/destination2.jpg"}'),
(3, 'Las Vegas', '', '{"image":"/images/destination3.jpg"}'),
(4, 'Dominician Republic', '', '{"image":"/images/destination4.jpg"}'),
(5, 'Cancun', '', '{"image":"/images/destination5.jpg"}'),
(6, 'Miami', '', '{"image":"/images/destination6.jpg"}'),
(7, 'Jamaica', '', '{"image":"/images/destination1.jpg"}'),
(8, 'New York City ', '', '{"image":"/images/destination2.jpg"}'),
(9, 'Las Vegas', '', '{"image":"/images/destination3.jpg"}'),
(10, 'Dominician Republic', '', '{"image":"/images/destination4.jpg"}'),
(11, 'Cancun', '', '{"image":"/images/destination5.jpg"}'),
(12, 'Miami', '', '{"image":"/images/destination6.jpg"}');

-- --------------------------------------------------------

--
-- Table structure for table `News`
--

CREATE TABLE IF NOT EXISTS `News` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `seoLink` varchar(255) CHARACTER SET utf8 NOT NULL,
  `text` text CHARACTER SET utf8 NOT NULL,
  `data` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '{"date": " ","image":" "}',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=30 ;

--
-- Dumping data for table `News`
--

INSERT INTO `News` (`id`, `parent_id`, `title`, `seoLink`, `text`, `data`) VALUES
(1, 0, 'Туры специально для вас', '/Aviamayak/news/best-weddings', 'hgsfd hgsh gfcsdhhfg', '{"date": " ","image":" "}	'),
(2, 1, '10 безвизовых стран, где гарантирован спокойный и безопасный отдых', '/Aviamayak/news/best-weddings/abc', 'Мечтаете о тихом и спокойном отпуске вдали от телевизора и работы? Aviasales собрал топ 10 направлений, куда россиянам не понадобится виза, а также лишние переживания о безопасности. Выбирайте отдых на свой вкус: райские острова, современные мегаполисы или ностальгия по Советскому Союзу.', '{"date": "10-12-2015 11:45","image":"/images/news1.jpg"}'),
(29, 1, 'Ведется запись на экскурсии', '/Aviamayak/news/best-weddings/def', 'Мучаетесь вопросом, откуда берутся дешевые авиабилеты? Приходите в наш тайский офис 2 или 3 декабря и собственными глазами убедитесь, как работает поисковик билетов Aviamayak.ru.\r\nПопасть на экскурсию предельно просто: Запишитесь на удобное время и дату. Не забудьте взять с собой хорошее настроение.\r\nСреди пришедших мы разыграем 3 ночи в пятизвездочном отеле Angsana Laguna Phuket.\r\n\r\nПриходите, будет весело! Только поторопитесь, количество мест ограничено.\r\n\r\nНе можете приехать сами? Расскажите друзьям. Просто поделитесь статьей в соцсетях.', '{"date": "10-12-2015 11:46","image":"/images/news2.jpg"}'),
(3, 1, 'Ведется запись на экскурсии', '/Aviamayak/news/best-weddings/def', 'Мучаетесь вопросом, откуда берутся дешевые авиабилеты? Приходите в наш тайский офис 2 или 3 декабря и собственными глазами убедитесь, как работает поисковик билетов Aviamayak.ru.\nПопасть на экскурсию предельно просто: Запишитесь на удобное время и дату. Не забудьте взять с собой хорошее настроение.\nСреди пришедших мы разыграем 3 ночи в пятизвездочном отеле Angsana Laguna Phuket.\n\nПриходите, будет весело! Только поторопитесь, количество мест ограничено.\n\nНе можете приехать сами? Расскажите друзьям. Просто поделитесь статьей в соцсетях.', '{"date": "10-12-2015 11:46","image":"/images/news2.jpg"}'),
(28, 1, '10 безвизовых стран, где гарантирован спокойный и безопасный отдых', '/Aviamayak/news/best-weddings/abc', 'Мечтаете о тихом и спокойном отпуске вдали от телевизора и работы? Aviasales собрал топ 10 направлений, куда россиянам не понадобится виза, а также лишние переживания о безопасности. Выбирайте отдых на свой вкус: райские острова, современные мегаполисы или ностальгия по Советскому Союзу.', '{"date": "10-12-2015 11:45","image":"/images/news1.jpg"}'),
(27, 1, 'Ведется запись на экскурсии', '/Aviamayak/news/best-weddings/def', 'Мучаетесь вопросом, откуда берутся дешевые авиабилеты? Приходите в наш тайский офис 2 или 3 декабря и собственными глазами убедитесь, как работает поисковик билетов Aviamayak.ru.\r\nПопасть на экскурсию предельно просто: Запишитесь на удобное время и дату. Не забудьте взять с собой хорошее настроение.\r\nСреди пришедших мы разыграем 3 ночи в пятизвездочном отеле Angsana Laguna Phuket.\r\n\r\nПриходите, будет весело! Только поторопитесь, количество мест ограничено.\r\n\r\nНе можете приехать сами? Расскажите друзьям. Просто поделитесь статьей в соцсетях.', '{"date": "10-12-2015 11:46","image":"/images/news2.jpg"}'),
(26, 1, '10 безвизовых стран, где гарантирован спокойный и безопасный отдых', '/Aviamayak/news/best-weddings/abc', 'Мечтаете о тихом и спокойном отпуске вдали от телевизора и работы? Aviasales собрал топ 10 направлений, куда россиянам не понадобится виза, а также лишние переживания о безопасности. Выбирайте отдых на свой вкус: райские острова, современные мегаполисы или ностальгия по Советскому Союзу.', '{"date": "10-12-2015 11:45","image":"/images/news1.jpg"}');

-- --------------------------------------------------------

--
-- Table structure for table `Pages`
--

CREATE TABLE IF NOT EXISTS `Pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` tinyint(4) NOT NULL,
  `icon` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `seoLink` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=27 ;

--
-- Dumping data for table `Pages`
--

INSERT INTO `Pages` (`id`, `parent_id`, `icon`, `title`, `seoLink`) VALUES
(1, 0, 'fa fa-book', 'О проекте', 'aboutproject'),
(2, 1, '', 'О компании', 'aboutcompany'),
(3, 1, '', 'Контакты', 'contacts'),
(4, 1, '', 'Наши партнеры', 'ourpartners'),
(5, 1, '', 'Новости', 'news'),
(6, 1, '', 'Вакансии', 'vacancies'),
(7, 0, 'fa fa-question-circle', 'Помощь', 'help'),
(8, 7, '', 'Как мы работаем', 'how-we-works'),
(9, 7, '', 'Конфиденциальность', 'confidence'),
(10, 7, '', 'Электронные авиабилеты', 'online-airtickets'),
(11, 7, '', 'Что такое чартеры', 'what-is-charters'),
(12, 7, '', 'Помощь (обратня связь)', 'tecnic-help'),
(13, 0, 'fa fa-users', 'Пользователям', 'forusers'),
(14, 13, '', 'Руководство для пользователя', 'users-book'),
(15, 13, '', 'Пользовательское соглашение', 'users-contract'),
(16, 13, '', 'Постоянным пользователям', 'for-main-users'),
(17, 13, '', 'Бонусная программа', 'bonus-programm'),
(18, 13, '', 'Акции', 'stocks'),
(19, 0, 'fa fa-list-alt', 'Услуги', 'services'),
(20, 19, '', 'Авиабилеты', 'airtickets'),
(21, 19, '', 'Билеты на чартер', 'charter-tickets'),
(22, 19, '', 'Отели', 'hotels'),
(23, 19, '', 'Туры', 'tours'),
(24, 19, '', 'Ж/Д билеты', 'train-tickets'),
(25, 19, '', 'Аренда авто', 'car-rent'),
(26, 19, '', 'Страхование для визы', 'visa-insurance');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
