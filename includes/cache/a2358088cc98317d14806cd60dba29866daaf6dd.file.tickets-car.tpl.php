<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-01-03 18:50:27
         compiled from "/vagrant/web/Aviamayak/modules/module_travelReservation/templates/tickets-car.tpl" */ ?>
<?php /*%%SmartyHeaderCode:147577526956892bec8f1c74-34823297%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a2358088cc98317d14806cd60dba29866daaf6dd' => 
    array (
      0 => '/vagrant/web/Aviamayak/modules/module_travelReservation/templates/tickets-car.tpl',
      1 => 1451847024,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '147577526956892bec8f1c74-34823297',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56892bec9993a4_32181052',
  'variables' => 
  array (
    'settings' => 0,
    'search' => 0,
    'inner' => 0,
    'additionsalElements' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56892bec9993a4_32181052')) {function content_56892bec9993a4_32181052($_smarty_tpl) {?><div class="module_travelReservation">
    <div class="search_block">
        <div class="inputs_block">
            <?php  $_smarty_tpl->tpl_vars['search'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['search']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['settings']->value['searches']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['search']->key => $_smarty_tpl->tpl_vars['search']->value) {
$_smarty_tpl->tpl_vars['search']->_loop = true;
?>
                <div class="<?php echo $_smarty_tpl->tpl_vars['search']->value['blockClass'];?>
 input_block">
                    <div class="input-field">
                        <input id="<?php echo $_smarty_tpl->tpl_vars['search']->value['id'];?>
"
                               type="<?php echo $_smarty_tpl->tpl_vars['search']->value['type'];?>
"
                                <?php if ($_smarty_tpl->tpl_vars['search']->value['activity']) {?>
                                    data-activity="<?php echo $_smarty_tpl->tpl_vars['search']->value['activity'];?>
"
                                <?php }?>
                                <?php if ($_smarty_tpl->tpl_vars['search']->value['data']) {?>
                                    data-data="<?php echo $_smarty_tpl->tpl_vars['search']->value['data'];?>
"
                                <?php }?>
                               class="<?php echo $_smarty_tpl->tpl_vars['search']->value['class'];?>
">
                        <label for="<?php echo $_smarty_tpl->tpl_vars['search']->value['id'];?>
">
                            <?php echo $_smarty_tpl->tpl_vars['search']->value['title'];?>

                        </label>
                <span class="search_icon">
                    <i class="<?php echo $_smarty_tpl->tpl_vars['search']->value['icon'];?>
"></i>
                </span>
                        <?php if ($_smarty_tpl->tpl_vars['search']->value['inner']) {?>
                            <?php echo $_smarty_tpl->tpl_vars['additionsalElements']->value[$_smarty_tpl->tpl_vars['inner']->value];?>

                        <?php }?>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="button_block">
            <div class="button_inner_block waves-effect">
                <button class="<?php echo $_smarty_tpl->tpl_vars['settings']->value['submit']['class'];?>
 waves-light">
                    <?php echo $_smarty_tpl->tpl_vars['settings']->value['submit']['title'];?>

                </button>
            </div>
        </div>
    </div>
</div> <?php }} ?>
