<div class="module_popular">
    <div class="popular_block first">
        <div class="popular_header">
            <div class="popular_block_title">
                {$destinations.title.title}
            </div>
            <div class="col s12 destinations-menu">
                <ul class="tabs">
                    {foreach from=$destinations.destinationsMenu item=menuItem}
                        <li class="tab col s3">
                            <a class="{$menuItem.class}" href="{$menuItem.seoLink}">
                            <span class="tabTitle">
                                {$menuItem.title}
                                </span>
                            <span class="tab_inner btn-floating btn-large waves-effect waves-light green">
                    <i class="{$menuItem.icon}"></i>
                        </span>
                            </a>
                        </li>
                    {/foreach}
                </ul>
            </div>
        </div>
    </div>
    <div class="popular_block second">
        <div class="popular_content">
            {foreach from=$destinations.destinationTabs item=tab}
                <div id="{$tab.id}" class="tabCol">
                    {foreach from=$tab.destinations item=destination}
                        <div class="popular_item waves-effect waves-light {$destination.class}">
                            <div class="popular_img" style="background-image: url('{$destination.image}');" title="{$destination.title}"></div>
                            <span class="popular_title">{$destination.title}</span>
                            <span class="popular_country">{$destination.country}</span>
                            {if $destination.subTitle != false}
                                <span class="popular_subTitle">{$destination.subTitle}</span>
                            {/if}
                            <span class="popular_season">{$destination.season}</span>
                        </div>
                    {/foreach}
                    <a class="waves-effect waves-light btn-large all_destinations">
                        {$destinations.button.title}
                    </a>
                </div>
            {/foreach}
        </div>
    </div>
</div>