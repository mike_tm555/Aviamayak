$(function() {
    $(".num-button").on("click", function() {
        var $button = $(this);
        var $input = $button.parent().find("input");
        var maxValue = parseFloat($input.attr('max'));
        var minValue = parseFloat($input.attr('min'));
        var oldValue = parseFloat($input.val());
        var newValue;
        if ($button.attr('id') == "inc" && oldValue < maxValue) {
                newValue = oldValue + 1;
        } else {
            if (oldValue > minValue) {
                newValue = oldValue - 1;
            } else {
                newValue = 0;
            }
        }
        $button.parent().find("input").val(newValue);
    });

});