$(document).ready(function(){
    var partners_slider = $('.partners_slider');
    $(partners_slider).owlCarousel({
        items:6,
        itemsDesktop : [1450, 6],
        autoPlay : true,
        lazyLoad: true,
        stopOnHover: true
    });
    $("#partners_button_left").click(function () {
        partners_slider.trigger('owl.next');
    });
    $("#partners_button_right").click(function () {
        partners_slider.trigger('owl.prev');
    });
});
