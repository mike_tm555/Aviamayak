<?php
class module_modals extends module
{
    protected function getData()
    {
        $getPlugin = GET_PLUGIN;

        $this->data['modals'] = $this->getModuleSettings();

        $this->data['order'] = plugin_order::getInstance()->$getPlugin('modal');

        if(!user::getInstance()->userLogged) {
            $this->data['user'] = plugin_user::getInstance()->$getPlugin('user_default');
        } else {
            $this->data['user'] = plugin_user::getInstance()->$getPlugin('user_logged');
        }
    }
}