$(document).ready(function(){
    var aboutUs_slider = $('.aboutUs_slider_block');
    $(aboutUs_slider).owlCarousel({
        items:3,
        itemsDesktop : [1450, 3],
        autoPlay : true,
        stopOnHover : true,
        lazyLoad: true
    });
    $("#aboutUs_button_left").click(function () {
        aboutUs_slider.trigger('owl.next');
    });
    $("#aboutUs_button_right").click(function () {
        aboutUs_slider.trigger('owl.prev');
    });
});
