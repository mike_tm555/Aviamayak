<div class="module_navigation">
    <div class="nav-prev">
    {foreach from=$navMenu.preNav item=menuItem}
    <a {if $menuItem.class != false}class="{$menuItem.class}"{/if}
       {if $menuItem.seoLink != false}href="{$menuItem.seoLink}"{/if}
       {if $menuItem.image != false}style="{$menuItem.image}"{/if}>
        <span class="inner" {if $menuItem.action != false}onclick="{$menuItem.action}"{/if}>
       {if $menuItem.icon !=false}<i class="{$menuItem.icon}"></i>{/if}
       {if $menuItem.title != false}{$menuItem.title}{/if}
        </span>
       {if $menuItem.pluginData != false}{${$menuItem.pluginData}}{/if}
    </a>
    {/foreach}
        </div>
    <nav>
        <div class="nav-wrapper">
            {foreach from=$navMenu.nav item=menuItem}
                {if $menuItem.list == false}
                <a {if $menuItem.class != false}class="{$menuItem.class}"{/if}
                   {if $menuItem.seoLink != false}href="{$menuItem.seoLink}"{/if}
                   {if $menuItem.image != false}style="{$menuItem.image}"{/if}
                   {if $menuItem.action != false}data-activates="{$menuItem.action}"{/if}>
                   {if $menuItem.icon !=false}<i class="{$menuItem.icon}"></i>{/if}
                   {if $menuItem.title != false}{$menuItem.title}{/if}
                </a>
                {else}
                    <ul class="{$menuItem.class}" id="{$menuItem.id}">
                        {foreach from=$menuItem.items item=listItem}
                        <li {if $listItem.class != false}class="{$listItem.class}"{/if}>
                            <a {if $listItem.image != false}style="{$listItem.image}"{/if}
                               {if $listItem.seoLink != false}href="{$listItem.seoLink}"{/if}
                               {if $listItem.action != false}data-activates="{$listItem.action}"{/if}>
                               {if $listItem.icon !=false}<i class="{$listItem.icon}"></i>{/if}
                               {if $listItem.title != false}{$listItem.title}{/if}
                            </a>
                        </li>
                        {/foreach}
                    </ul>
                {/if}
            {/foreach}
        </div>
    </nav>
    <div class="nav-after">
        {foreach from=$navMenu.afterNav item=menuItem}
            <a id="{$menuItem.name}"
               {if $menuItem.class != false}class="{$menuItem.class}"{/if}
               {if $menuItem.seoLink != false}href="{$menuItem.seoLink}"{/if}
               {if $menuItem.image != false}style="{$menuItem.image}"{/if}>
        <span class="inner" {if $menuItem.action != false}onclick="{$menuItem.action}"{/if}>
       {if $menuItem.icon !=false}<i class="{$menuItem.icon}"></i>{/if}
            {if $menuItem.title != false}{$menuItem.title}{/if}
        </span>
                {if $menuItem.pluginData != false}{${$menuItem.pluginData}}{/if}
            </a>
        {/foreach}
    </div>
</div>