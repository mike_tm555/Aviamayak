<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-12-22 05:48:28
         compiled from "/vagrant/web/Aviamayak/modules/module_partners/templates/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:18805980865678e42c7f54c0-49590853%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '52413cb76537b0fcfc730fa1a345adb57b345a03' => 
    array (
      0 => '/vagrant/web/Aviamayak/modules/module_partners/templates/index.tpl',
      1 => 1449696061,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18805980865678e42c7f54c0-49590853',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'partners' => 0,
    'partner' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5678e42c889791_57654437',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5678e42c889791_57654437')) {function content_5678e42c889791_57654437($_smarty_tpl) {?><div class="module_partners">
    <div class="partners_slider_block">
        <span id="partners_button_left" class="slide_button button_left"><i class="fa fa-angle-left"></i></span>
        <span id="partners_button_right" class="slide_button button_right"><i class="fa fa-angle-right"></i></span>
        <div class="partners_slider">
            <?php  $_smarty_tpl->tpl_vars['partner'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['partner']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['partners']->value['partners']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['partner']->key => $_smarty_tpl->tpl_vars['partner']->value) {
$_smarty_tpl->tpl_vars['partner']->_loop = true;
?>
                <div class="slider_item lazyOwl" <?php if ($_smarty_tpl->tpl_vars['partner']->value['image']) {?>data-src="<?php echo $_smarty_tpl->tpl_vars['partner']->value['image'];?>
"<?php }?>>
                    <?php if ($_smarty_tpl->tpl_vars['partner']->value['banner']) {?>
                        <?php echo $_smarty_tpl->tpl_vars['partner']->value['banner'];?>

                    <?php } else { ?>
                        <?php if ($_smarty_tpl->tpl_vars['partner']->value['title']) {?>
                            <span class="partner_title"><?php echo $_smarty_tpl->tpl_vars['partner']->value['title'];?>
</span>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['partner']->value['icon']) {?>
                            <span class="<?php if (!$_smarty_tpl->tpl_vars['partner']->value['title']) {?>only_icon<?php } else { ?>and_icon<?php }?> partner_icon">
                                <i class="<?php echo $_smarty_tpl->tpl_vars['partner']->value['icon'];?>
"></i>
                            </span>
                        <?php }?>
                    <?php }?>
                </div>
            <?php } ?>
        </div>
    </div>
</div><?php }} ?>
