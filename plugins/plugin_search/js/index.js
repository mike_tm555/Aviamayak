function openElasticSearch(){
    var search_block = document.getElementById('elastic-search'),
        open_search_class = 'open-search';
    $(search_block).fadeIn(50);
    $(search_block).addClass(open_search_class);
}
function closeElasticSearch() {
    var search_block = document.getElementById('elastic-search'),
        open_search_class = 'open-search';
    $(search_block).removeClass(open_search_class);
    $(search_block).fadeOut(50);

}