<div class="module_follow">
    <div class="title">
        {$follow.title}
    </div>
    <div class="text">
        {$follow.text}
    </div>
    <div class="follow_form">
        {$followForm}
    </div>
</div>