<?php
class module
{
    protected
        $name,
        $data = null,
        $template = 'index',
        $method='display',
        $json='index',
        $tickets;

    protected static $instance;

    public static function getInstance()
    {
        $child_class =  get_called_class();
        if (!self::$instance || self::$instance != $child_class) {
            self::$instance = new $child_class();
        }
        return self::$instance;
    }

    protected function __construct()
    {
        $this->name = get_called_class();
        $this->data['redact'] = page::getInstance()->redact;
        if(page::getInstance()->component == SERVICES) {
            $this->tickets = page::getInstance()->page;
        } else {
            $this->tickets = ACTIVE_TICKETS;
        }
    }

    protected function getModuleSettings($json=false)
    {
        $jsonFile = (!$json) ? $this->json.JSON : $json.JSON;
        return json_decode(file_get_contents(MODULES_PATH.$this->name.SETTINGS_PATH.$jsonFile),true);
    }

    protected function dataDecoder($data)
    {
        if(is_array($data)) {
            foreach($data as $key=>$value) {
                if(strpos($value, JSON_IDENTIFIER) !== false) {
                    $data[$key] = json_decode($value, true);
                } else {
                    $data[$key] = $value;
                }
            }
        } else {
            $data = json_decode($data,true);
        }
        return $data;
    }

    public function printModule()
    {
        $getDataFunction = GET_DATA_FUNC;
        $this->$getDataFunction();
        if(isset($this->data['template'])){
            $this->template = $this->data['template'];
        }
        if(isset($this->data['method'])){
            $this->method = $this->data['method'];
        }
        smartyTpl::loadSmarty($this->name,$this->data,$this->template,$this->method);
    }
}