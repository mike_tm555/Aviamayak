<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-01-02 09:50:04
         compiled from "/vagrant/web/Aviamayak/plugins/plugin_searches/templates/tickets-flight.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1448620673567de49b7e8c16-41345111%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9fdaaac6e3265a3d5022f5cc0bca6ec44fc3ecc6' => 
    array (
      0 => '/vagrant/web/Aviamayak/plugins/plugin_searches/templates/tickets-flight.tpl',
      1 => 1451728201,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1448620673567de49b7e8c16-41345111',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_567de49b81d0f2_19487479',
  'variables' => 
  array (
    'settings' => 0,
    'search' => 0,
    'inner' => 0,
    'additionsalElements' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_567de49b81d0f2_19487479')) {function content_567de49b81d0f2_19487479($_smarty_tpl) {?><div class="search_block">
    <div class="inputs_block">
        <?php  $_smarty_tpl->tpl_vars['search'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['search']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['settings']->value['searches']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['search']->key => $_smarty_tpl->tpl_vars['search']->value) {
$_smarty_tpl->tpl_vars['search']->_loop = true;
?>
            <div class="<?php echo $_smarty_tpl->tpl_vars['search']->value['blockClass'];?>
 input_block">
                <div class="input-field">
                    <input id="<?php echo $_smarty_tpl->tpl_vars['search']->value['id'];?>
"
                           type="<?php echo $_smarty_tpl->tpl_vars['search']->value['type'];?>
"
                            <?php if ($_smarty_tpl->tpl_vars['search']->value['activity']) {?>
                                data-activity="<?php echo $_smarty_tpl->tpl_vars['search']->value['activity'];?>
"
                            <?php }?>
                            <?php if ($_smarty_tpl->tpl_vars['search']->value['data']) {?>
                                data-data="<?php echo $_smarty_tpl->tpl_vars['search']->value['data'];?>
"
                            <?php }?>
                           class="<?php echo $_smarty_tpl->tpl_vars['search']->value['class'];?>
">
                    <label for="<?php echo $_smarty_tpl->tpl_vars['search']->value['id'];?>
">
                        <?php echo $_smarty_tpl->tpl_vars['search']->value['title'];?>

                    </label>
                <span class="search_icon">
                    <i class="<?php echo $_smarty_tpl->tpl_vars['search']->value['icon'];?>
"></i>
                </span>
                    <?php if ($_smarty_tpl->tpl_vars['search']->value['inner']) {?>
                        <?php echo $_smarty_tpl->tpl_vars['additionsalElements']->value[$_smarty_tpl->tpl_vars['inner']->value];?>

                    <?php }?>
                </div>
            </div>
        <?php } ?>
    </div>
    <div class="button_block">
        <div class="button_inner_block">
            <button class="<?php echo $_smarty_tpl->tpl_vars['settings']->value['submit']['class'];?>
">
                <?php echo $_smarty_tpl->tpl_vars['settings']->value['submit']['title'];?>

            </button>
        </div>
    </div>
</div>
<div class='external_plugin'
     id='__biletix__wl__'
     data-plugins='preloader_crossing_shapes'
     data-theme-custom='220'>
</div>
<?php echo '<script'; ?>
 id='__biletix__wl__script'
        type='text/javascript'
        src='https://ps.biletix.ru/static/wl/build/biletix_wl.min.js'
        async>
<?php echo '</script'; ?>
><?php }} ?>
