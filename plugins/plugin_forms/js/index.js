function Form(formID,formOptions) {

    var _self = this;

    this.options = {};

    this.messages = {
        us: {
            required: 'The %s field is required.',
            matches: 'The %s field does not match the %s field.',
            "default": 'The %s field is still set to default, please change.',
            valid_email: 'The %s field must contain a valid email address.',
            valid_emails: 'The %s field must contain all valid email addresses.',
            mobile_number: 'The %s field must contain all valid phone number.',
            min_length: 'The %s field must be at least %s characters in length.',
            max_length: 'The %s field must not exceed %s characters in length.',
            exact_length: 'The %s field must be exactly %s characters in length.',
            greater_than: 'The %s field must contain a number greater than %s.',
            less_than: 'The %s field must contain a number less than %s.',
            alpha: 'The %s field must only contain alphabetical characters.',
            alpha_numeric: 'The %s field must only contain alpha-numeric characters.',
            alpha_dash: 'The %s field must only contain alpha-numeric characters, underscores, and dashes.',
            numeric: 'The %s field must contain only numbers.',
            integer: 'The %s field must contain an integer.',
            decimal: 'The %s field must contain a decimal number.',
            is_natural: 'The %s field must contain only positive numbers.',
            is_natural_no_zero: 'The %s field must contain a number greater than zero.',
            valid_ip: 'The %s field must contain a valid IP.',
            valid_base64: 'The %s field must contain a base64 string.',
            valid_credit_card: 'The %s field must contain a valid credit card number.',
            is_file_type: 'The %s field must contain only %s files.',
            valid_url: 'The %s field must contain a valid URL.',
            greater_than_date: 'The %s field must contain a more recent date than %s.',
            less_than_date: 'The %s field must contain an older date than %s.',
            greater_than_or_equal_date: 'The %s field must contain a date that\'s at least as recent as %s.',
            less_than_or_equal_date: 'The %s field must contain a date that\'s %s or older.',
            new_error: 'An error has occurred with the %s field.'
        },
        am: {
            required: '%s դաշտը ենթակա է պարտադիր լրացման',
            matches: '%s դաշտի արժեքը չի համընկնում  %s դաշտի արժեքի հետ',
            "default": '%s դաշտի արժեքը ընտրված չէ',
            valid_email: '%s դաշտը պետք է պարունակի ճիշտ email հասցե',
            valid_emails: ' %s  դաշտը պետք է պարունակի ճիշտ email հասցեներ',
            mobile_number: '%s դաշտը պետք է պարունակի ճիշտ  հեռախոսահամար',
            min_length: '%s դաշտը պետք է պարունակի առնվազն %s սիմվոլ',
            max_length: '%s դաշտը պետք է պարունակի  առավելագույնը %s սիմվոլ',
            exact_length: '%s դաշտը պետք է պարունակի %s սիմվոլներ',
            greater_than: '%s դաշտը պետք է պարունակի թիվ մեծ %s ֊ից',
            less_than: '%s դաշտը պետք է պարունակի թիվ փոքր %s ֊ից',
            alpha: '%s դաշտը պետք է պարունակի միայն տառային սիմվոլներ',
            alpha_numeric: '%s դաշտը պետք է պարունակի միայն տառային և թվային սիմվոլներ',
            alpha_dash: '%s դաշտը պետք է պարունակի միայն տառային և թվային և ընդգծման սիմվոլներ',
            numeric: '%s դաշտը պետք է պարունակի միայն թվային սիմվոլներ',
            integer: '%s դաշտը պետք է պարունակի միայն ամբողջ թվեր',
            decimal: '%s դաշտը պետք է պարունակի միայն 10֊ական թվեր',
            is_natural: '%s դաշտը պետք է պարունակի միայն դրական թվեր',
            is_natural_no_zero: '%s դաշտը պետք է պարունակի միայն դրական թվեր',
            valid_ip: '%s դաշտը պետք է պարունակի միայն ճիշտ IP հասցե',
            valid_base64: '%s դաշտը պետք է պարունակի base64 կոդ',
            valid_credit_card: '%s դաշտը պետք է պարունակի վճարային քարտի ճիշտ համար',
            is_file_type: '%s դաշտը պետք է պարունակի միայն %s ֆայլեր',
            valid_url: '%s դաշտը պետք է պարունակի ճիշտ URL հասցե',
            greater_than_date: '%s դաշտը պետք է պարունակի ավելի նոր ամսաթիվ քան %s.',
            less_than_date: '%s դաշտը պետք է պարունակի ավելի հին ամսաթիվ քան %s.',
            greater_than_or_equal_date: '%s դաշտը պետք է պարունակի ավելի նոր ամսաթիվ քան %s, կամ հենց նույն ամսաթիվը',
            less_than_or_equal_date: '%s դաշտը պետք է պարունակի ավելի հին ամսաթիվ քան %s կամ հենց նույն ամսաթիվը',
            new_error: '%s դաշտի ստուգման ժամանակ առաջացավ սխալ',
            existing_email: 'Նշված E-mail հասցեով օգտատեր արդեն գոյություն ունի',
            existing_mobile: 'Նշված հերախոսահամարով օգտատեր արդեն գոյություն ունի'
        },
        ru: {
            required: '%s դաշտը ենթակա է պարտադիր լրացման',
            matches: '%s դաշտի արժեքը չի համընկնում  %s դաշտի արժեքի հետ',
            "default": '%s դաշտի արժեքը ընտրված չէ',
            valid_email: '%s դաշտը պետք է պարունակի ճիշտ email հասցե',
            valid_emails: ' %s  դաշտը պետք է պարունակի ճիշտ email հասցեներ',
            mobile_number: '%s դաշտը պետք է պարունակի ճիշտ  հեռախոսահամար',
            min_length: '%s դաշտը պետք է պարունակի առնվազն %s սիմվոլ',
            max_length: '%s դաշտը պետք է պարունակի  առավելագույնը %s սիմվոլ',
            exact_length: '%s դաշտը պետք է պարունակի %s սիմվոլներ',
            greater_than: '%s դաշտը պետք է պարունակի թիվ մեծ %s ֊ից',
            less_than: '%s դաշտը պետք է պարունակի թիվ փոքր %s ֊ից',
            alpha: '%s դաշտը պետք է պարունակի միայն տառային սիմվոլներ',
            alpha_numeric: '%s դաշտը պետք է պարունակի միայն տառային և թվային սիմվոլներ',
            alpha_dash: '%s դաշտը պետք է պարունակի միայն տառային և թվային և ընդգծման սիմվոլներ',
            numeric: '%s դաշտը պետք է պարունակի միայն թվային սիմվոլներ',
            integer: '%s դաշտը պետք է պարունակի միայն ամբողջ թվեր',
            decimal: '%s դաշտը պետք է պարունակի միայն 10֊ական թվեր',
            is_natural: '%s դաշտը պետք է պարունակի միայն դրական թվեր',
            is_natural_no_zero: '%s դաշտը պետք է պարունակի միայն դրական թվեր',
            valid_ip: '%s դաշտը պետք է պարունակի միայն ճիշտ IP հասցե',
            valid_base64: '%s դաշտը պետք է պարունակի base64 կոդ',
            valid_credit_card: '%s դաշտը պետք է պարունակի վճարային քարտի ճիշտ համար',
            is_file_type: '%s դաշտը պետք է պարունակի միայն %s ֆայլեր',
            valid_url: '%s դաշտը պետք է պարունակի ճիշտ URL հասցե',
            greater_than_date: '%s դաշտը պետք է պարունակի ավելի նոր ամսաթիվ քան %s.',
            less_than_date: '%s դաշտը պետք է պարունակի ավելի հին ամսաթիվ քան %s.',
            greater_than_or_equal_date: '%s դաշտը պետք է պարունակի ավելի նոր ամսաթիվ քան %s, կամ հենց նույն ամսաթիվը',
            less_than_or_equal_date: '%s դաշտը պետք է պարունակի ավելի հին ամսաթիվ քան %s կամ հենց նույն ամսաթիվը',
            new_error: '%s դաշտի ստուգման ժամանակ առաջացավ սխալ',
            existing_email: 'Նշված E-mail հասցեով օգտատեր արդեն գոյություն ունի',
            existing_mobile: 'Նշված հերախոսահամարով օգտատեր արդեն գոյություն ունի'
        }
    };

    this.rules = {
        ruleRegex: /^(.+?)\[(.+)\]$/,
        numericRegex: /^[0-9]+$/,
        mobileNumberRegex: /^([+0-9]{1,3})?([0-9]{10,11})$/i,
        integerRegex: /^\-?[0-9]+$/,
        decimalRegex: /^\-?[0-9]*\.?[0-9]+$/,
        emailRegex: /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
        alphaRegex: /^[a-zа-я]+$/i,
        alphaNumericRegex: /^[a-zа-я0-9]+$/i,
        alphaDashRegex: /^[a-zа-я0-9_\-]+$/i,
        naturalRegex: /^[0-9]+$/i,
        naturalNoZeroRegex: /^[1-9][0-9]*$/i,
        ipRegex: /^((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){3}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})$/i,
        base64Regex: /[^a-zA-Z0-9\/\+=]/i,
        numericDashRegex: /^[\d\-\s]+$/,
        urlRegex: /^((http|https):\/\/(\w+:{0,1}\w*@)?(\S+)|)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?$/,
        dateRegex: /\d{4}-\d{1,2}-\d{1,2}/,
        dateTimeRegex: /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)/,
        timeRegex: /([01]\d|2[0-3]):([0-5]\d)/
    };

    this.hooks = {
        getValidDate: function (date) {
            if (!date.match('today') && !date.match(this.dateRegex)) {
                return false;
            }

            var validDate = new Date(),
                validDateArray;

            if (!date.match('today')) {
                validDateArray = date.split('-');
                validDate.setFullYear(validDateArray[0]);
                validDate.setMonth(validDateArray[1] - 1);
                validDate.setDate(validDateArray[2]);
            }
            return validDate;
        },
        required: function (field) {
            var value = field.value;
            if (field.type === 'checkbox' || field.type === 'radio') {
                return (field.checked === true);
            }
            return (value !== null && value !== '');
        },

        defaultField: function (field, defaultName) {
            return field.value !== defaultName;
        },

        matches: function (field, matchName) {
            var el = _self.GetElementInsideContainer(matchName);
            if (el) {
                return field.value === el.value;
            }
            return false;
        },

        mobileNumber: function (field) {
            return _self.rules.mobileNumberRegex.test(field.value);
        },

        valid_email: function (field) {
            return _self.rules.emailRegex.test(field.value);
        },

        valid_emails: function (field) {
            var result = field.value.split(/\s*,\s*/g);
            for (var i = 0, resultLength = result.length; i < resultLength; i++) {
                if (!_self.rules.emailRegex.test(result[i])) {
                    return false;
                }
            }
            return true;
        },

        min_length: function (field, length) {
            if (!_self.rules.numericRegex.test(length)) {
                return false;
            }
            return (field.value.length >= parseInt(length, 10));
        },

        max_length: function (field, length) {
            if (!_self.rules.numericRegex.test(length)) {
                return false;
            }
            return (field.value.length <= parseInt(length, 10));
        },

        exact_length: function (field, length) {
            if (!_self.rules.numericRegex.test(length)) {
                return false;
            }
            return (field.value.length === parseInt(length, 10));
        },

        greater_than: function (field, param) {
            if (!_self.rules.decimalRegex.test(field.value)) {
                return false;
            }
            return (parseFloat(field.value) > parseFloat(param));
        },

        less_than: function (field, param) {
            if (!_self.rules.decimalRegex.test(field.value)) {
                return false;
            }
            return (parseFloat(field.value) < parseFloat(param));
        },

        alpha: function (field) {
            return (_self.rules.alphaRegex.test(field.value));
        },

        alpha_numeric: function (field) {
            return (_self.rules.alphaNumericRegex.test(field.value));
        },

        alpha_dash: function (field) {
            return (_self.rules.alphaDashRegex.test(field.value));
        },

        numeric: function (field) {
            return (_self.rules.numericRegex.test(field.value));
        },

        integer: function (field) {
            return (_self.rules.integerRegex.test(field.value));
        },

        decimal: function (field) {
            return (_self.rules.decimalRegex.test(field.value));
        },

        is_natural: function (field) {
            return (_self.rules.naturalRegex.test(field.value));
        },

        is_natural_no_zero: function (field) {
            return (_self.rules.naturalNoZeroRegex.test(field.value));
        },

        valid_ip: function (field) {
            return (_self.rules.ipRegex.test(field.value));
        },

        valid_base64: function (field) {
            return (_self.rules.base64Regex.test(field.value));
        },

        valid_url: function (field) {
            return (_self.rules.urlRegex.test(field.value));
        },

        valid_credit_card: function (field) {
            if (!_self.rules.numericDashRegex.test(field.value)) return false;
            var nCheck = 0, nDigit = 0, bEven = false;
            var strippedField = field.value.replace(/\D/g, "");
            for (var n = strippedField.length - 1; n >= 0; n--) {
                var cDigit = strippedField.charAt(n);
                nDigit = parseInt(cDigit, 10);
                if (bEven) {
                    if ((nDigit *= 2) > 9) nDigit -= 9;
                }
                nCheck += nDigit;
                bEven = !bEven;
            }
            return (nCheck % 10) === 0;
        },

        is_file_type: function (field, type) {
            if (field.type !== 'file') {
                return true;
            }
            var ext = field.value.substr((field.value.lastIndexOf('.') + 1)),
                typeArray = type.split(','),
                inArray = false,
                i = 0,
                len = typeArray.length;
            for (i; i < len; i++) {
                if (ext == typeArray[i]) inArray = true;
            }
            return inArray;
        },

        greater_than_date: function (field, date) {
            var enteredDate = this.getValidDate(field.value),
                validDate = this.getValidDate(date);
            if (!validDate || !enteredDate) {
                return false;
            }
            return enteredDate > validDate;
        },

        less_than_date: function (field, date) {
            var enteredDate = this.getValidDate(field.value),
                validDate = this.getValidDate(date);

            if (!validDate || !enteredDate) {
                return false;
            }
            return enteredDate < validDate;
        },

        greater_than_or_equal_date: function (field, date) {
            var enteredDate = this.getValidDate(field.value),
                validDate = this.getValidDate(date);
            if (!validDate || !enteredDate) {
                return false;
            }
            return enteredDate >= validDate;
        },

        less_than_or_equal_date: function (field, date) {
            var enteredDate = this.getValidDate(field.value),
                validDate = this.getValidDate(date);

            if (!validDate || !enteredDate) {
                return false;
            }
            return enteredDate <= validDate;
        },

        existing_email: function() {
            return _self.messages['am']['existing_email'];
        },

        existing_mobile: function() {
            return _self.messages['am']['existing_mobile'];
        }
    };

    this.setFormOptions = function() {
        this.options = {
            errorClass: formOptions.errorClass || 'error',
            errorBlock: formOptions.errorBlock || 'error_block',
            errorPrefix: formOptions.errorPrefix || 'error_',
            dataFieldClass: formOptions.dataFieldClass || 'data',
            formStepsClass: formOptions.formStepsClass || 'form_step',
            formStepSubmitClass: formOptions.formStepSubmitClass || 'step_submit',
            formID: formID,
            formLanguage: document.getElementById(formID).getAttribute('data-language'),
            formName: document.getElementById(formID).getAttribute('data-name'),
            formAjax: document.getElementById(formID).getAttribute('data-ajax'),
            formMethod: document.getElementById(formID).getAttribute('method'),
            formAction: document.getElementById(formID).getAttribute('action'),
            formStep: [],
            errors: [],
            data: {},
            fields: {}
        };
        var formSteps = document.getElementById(this.options.formID).getElementsByClassName(this.options.formStepsClass);
        for (var i = 0; i < formSteps.length; i++) {
            var step = formSteps[i].id;
            this.options.formStep[i] = {
                stepID: step,
                controller: document.getElementById(step).getAttribute('data-controller'),
                action: document.getElementById(step).getAttribute('data-action')
            };
            var stepFields = document.getElementById(step).getElementsByClassName(this.options.dataFieldClass);
            for (var j = 0; j < stepFields.length; j++) {
                this.getField(stepFields[j].name);
            }
        }
    };

    this.GetElementInsideContainer = function(childID) {
        var elm = document.getElementById(childID);
        var parent = elm ? elm.parentNode : {};
        return (parent.id && parent.id === this.options.formID) ? elm : {};
    };

    this.getField = function(name,newRule) {
        var responseRule = newRule || false;
        var field = document.getElementsByName(name);
        var checked = "input[name='"+name+"']";
        var value = '';
        if($(field).attr('type') === 'radio') {
            $(checked).is(":checked") === true ? value = $(checked+":checked").val() : value = '';
        } else {
            value = $(field).val();
        }
        var rules;
        var fieldRule = $(field).attr('data-rules');
        if(responseRule == false){
            rules = fieldRule || '';
        } else {
            if(fieldRule == '') {
                rules = responseRule;
            } else {
                rules = fieldRule + '|' + responseRule;
            }
        }
        this.options.fields[name] = {
            id: $(field).attr('id'),
            name: name,
            step: $(field).attr('data-step'),
            display: $(field).attr('data-display') || name,
            rules: rules,
            depends: $(field).attr('data-depends'),
            element: null,
            type: $(field).attr('type'),
            value: value,
            checked: $(checked).is(":checked")
        };
        this.options.data[name] = value;
    };

    this.validateField = function(fieldName,newRule) {
        var rule = newRule || false;
        this.getField(fieldName,rule);
        this.resetErrors(fieldName);

        var field = this.options.fields[fieldName];
        var rules = field.rules.split('|'),
            indexOfRequired = field.rules.indexOf('required'),
            isEmpty = (!field.value || field.value === '' || field.value === undefined || field.value === null);

        var errorMessages = [];
        for (var i = 0, ruleLength = rules.length; i < ruleLength; i++) {

            var method = rules[i],
                param = null,
                failed = false,
                parts = this.rules['ruleRegex'].exec(method);

            if (indexOfRequired === -1 && isEmpty) {
                continue;
            }

            if (parts) {
                method = parts[1];
                param = parts[2];
            }

            if (method.charAt(0) === '!') {
                method = method.substring(1, method.length);
            }

            if (typeof this.hooks[method] === 'function') {
                if (!this.hooks[method].apply(this, [field, param])) {
                    failed = true;
                }
            } else if (method.substring(0, 9) === 'callback_') {
                method = method.substring(9, method.length);

                if (typeof this.handlers[method] === 'function') {
                    if (this.handlers[method].apply(this, [field.value, param, field]) === false) {
                        failed = true;
                    }
                }
            }

            if (failed) {
                var lang = this.options.formLanguage;
                var source = this.messages[lang][method],
                    message = this.messages[lang]['new_error'].replace('%s', field.display);

                if (source) {
                    message = source.replace('%s', field.display);
                    if (param) {
                        message = message.replace('%s', (field[param]) ? field[param].display : param);
                    }
                }
                errorMessages.push(message);
            }
        }
        if(errorMessages.length > 0){
            var fieldId = field.name;
            this.options.errors[fieldId] = errorMessages.join('|');
            this.parsingErrors(field.name);
        }
    };

    this.setDisplay = function(obj, type){
        $(obj).css('display', type);
    };

    this.parsingErrors = function(fieldName) {
        if (this.options.errors[fieldName] != undefined && this.options.errors[fieldName] != 'undefined') {
            var errors = this.options.errors[fieldName].split('|');
            var errorString = '';
            for(var i = 0; i < errors.length; i++) {
                if(errors[i] != undefined && errors[i] != 'undefined') {
                    errorString += errors[i] + "</br>";
                }
            }
            var field = document.getElementsByName(fieldName);
            var errorBlock = document.createElement("span");
            errorBlock.className = this.options.errorBlock +' '+ this.options.errorPrefix + fieldName;
            $(errorBlock).html('<span class="error_triangle"></span>' + errorString);
            $(field).addClass(this.options.errorClass).before(errorBlock);
            _self.setDisplay($('.'+this.options.errorBlock), 'none');
            $(field).focus(function(){$(errorBlock).fadeIn(300,function(){_self.setDisplay(errorBlock,'block')})});
            $(field).focusout(function(){$(errorBlock).fadeOut(300,function(){_self.setDisplay(errorBlock,'none')})});
        }
    };

    this.resetErrors = function(fieldName) {
        $('.'+this.options.errorPrefix+fieldName).fadeOut(300, function(){ $(this).remove(); });
        if (Object.keys(this.options.errors).length > 0) {
            delete this.options.errors[fieldName];
        }
        var targetDiv = document.getElementsByName(fieldName);
        $(targetDiv).removeClass(this.options.errorClass);
    };

    this.formRequest = function(data) {
        var formData = JSON.stringify(data);
        xhr({controller: data.controller, rule: data.action, htmlResponse:false, data:formData},
            function(response) {

            },function(response){

            });
    };

    this.stepSubmit = function(step) {
        var stepFields = document.getElementById(step).getElementsByClassName(this.options.dataFieldClass);
        for (var i = 0; i < stepFields.length; i++) {
            this.validateField(stepFields[i].name);
        }

        if(Object.keys(this.options.errors).length === 0) {
            for (var j = 0; j < this.options.formStep.length; j++) {
                if(step == this.options.formStep[j].stepID) {
                    if(this.options.formStep[j].controller != null || this.options.formStep[j].controller != 0 || this.options.formStep[j].controller != '') {
                        this.options.data['controller'] = this.options.formStep[j].controller;
                        this.options.data['action'] = this.options.formStep[j].action;
                        this.formRequest(this.options.data);
                    }
                }
            }
        }
        else {
            var fieldsWithErrors = document.getElementsByClassName(this.options.errorClass);
            $(fieldsWithErrors[0]).focus();
        }
    };

    this.formSubmit = function() {
        var formSteps = document.getElementById(this.options.formID).getElementsByClassName(this.options.formStepsClass);
        for (var i = 0; i < formSteps.length; i++) {
            this.stepSubmit(formSteps[i].id);
        }
    };

    this.setListeners = function() {
        $('#'+this.options.formID + ' .' + this.options.formStepSubmitClass).click(function(e){
            if(_self.options.formStep.length = 1) {
                e.preventDefault();
                _self.formSubmit();
            }
            else {
                e.preventDefault();
                _self.stepSubmit($(this).parents('.'+_self.options.formStepsClass).attr('id'));
            }
        });

        $('#' + this.options.formID + ' .'+this.options.dataFieldClass).change(function(){
            var fieldName = $(this).attr('name');
            _self.validateField(fieldName);
        });
    };

    this.setFormOptions();

    this.setListeners();
}

$.fn.extend({
    formController:function(options) {
        options = options || {};
        var id = $(this).attr('id');
        return new Form(id,options);
    }
});

$(document).ready(function() {
    $('.form').each(function(){
        $(this).formController();
    });
});