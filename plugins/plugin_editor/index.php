<?php
class plugin_editor extends plugin
{
    protected
        $table = "Pages",
        $subTable = "Contents";

    public function addContent($data)
    {
        $this->data['editor'] = $this->getPluginSettings();
        $data = $this->dataDecoder($data['data']);
        db::getInstance()->orderBy('position','DESC');
        $position = db::getInstance()->get_field($this->subTable,"page_id = '".$data['page_id']."'",'position');
        db::getInstance()->resetConditions();
        $newContentId = db::getInstance()->insert($this->subTable,array(
            'page_id'=>$data['page_id'],
            'class'=>$data['class'],
            'position'=>$position + 1
        ));

        if($newContentId) {
            echo 'true';
        } else {
            echo $this->data['editor']['errors']['errorAddNewContent'];
        }
    }

    public function deleteContent($data)
    {
        $this->data['editor'] = $this->getPluginSettings();
        $data = $this->dataDecoder($data['data']);
        $deleteContent = db::getInstance()->delete($this->subTable,"id = '".$data['content_id']."'");
        if($deleteContent) {
            echo 'true';
        } else {
            echo $this->data['editor']['errors']['errorAddNewContent'];
        }
    }

    public function changeContentSize($data)
    {
        $this->data['editor'] = $this->getPluginSettings();
        $data = $this->dataDecoder($data['data']);
        $deleteContent = db::getInstance()->update($this->subTable,array('class'=>$data['changingValue']),$data['content_id']);
        if($deleteContent) {
            echo 'true';
        } else {
            echo $this->data['editor']['errors']['errorChangingContent'];
        }
    }

    public function changeContentPosition($data)
    {
        $this->data['editor'] = $this->getPluginSettings();
        $data = $this->dataDecoder($data['data']);
        $deleteContent = db::getInstance()->update($this->subTable,array('position'=>$data['changingValue']),$data['content_id']);
        if($deleteContent) {
            echo 'true';
        } else {
            echo $this->data['editor']['errors']['errorChangingContent'];
        }
    }

    public function saveContentText($data)
    {
        $this->data['editor'] = $this->getPluginSettings();
        $data = $this->dataDecoder($data['data']);
        $deleteContent = db::getInstance()->update($this->subTable,array('content'=>$data['changingValue']),$data['content_id']);
        if($deleteContent) {
            echo 'true';
        } else {
            echo $this->data['editor']['errors']['errorChangingContent'];
        }
    }

}