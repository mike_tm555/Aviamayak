<div class="passengers-block" id="passengers-charter">
    <i class="pointer"></i>
    <div class="passengers-block-passengers">
        {foreach from=$settings.passengers.items item=item}
            <div class="passengers-item">
                <span class="passengers-title">{$item.title}</span>
                <div class="passengers-input-block">
                <span class="passengers-minus" onclick="AVM.TravelReservation.Passengers.countChange('minus','{$item.id}')">
                    <i class="fa fa-minus"></i>
                </span>

                    <input id="{$item.id}"
                           type="number"
                           readonly="readonly"
                           class="passengers-input"
                           min="{$item.min}"
                           max="{$item.max}"
                           value="{$item.default}"/>

                <span class="passengers-plus" onclick="AVM.TravelReservation.Passengers.countChange('plus','{$item.id}')">
                    <i class="fa fa-plus"></i>
                </span>
                </div>
            </div>
        {/foreach}
    </div>
    <div class="passenger-block-classes">
        <input type="checkbox"
               class="filled-in"
               id="passenger-class-type"
               data-checked-data="{$settings.classes.checked.data}"
               data-unchecked-data="{$settings.classes.unchecked.data}"
               data-checked-title="{$settings.classes.checked.title}"
               data-unchecked-title="{$settings.classes.unchecked.title}"/>
        <label for="passenger-class-type" class="passengers-classes-label">
            {$settings.classes.default}
        </label>
    </div>
</div>