if(__REDACT__ == '1') {

    function addContent() {
        this.data = {};
        var activeSubPage  =  $('.active'),
            self = this;
        this.data['page_id'] = $(activeSubPage).attr('data-page');
        this.data['class'] = 'big_content';
        var data = JSON.stringify(this.data);
        xhr({controller:'editor',rule:'addContent',htmlResponse:false,data:data},
            function(response){
                if(response == true) {
                    location.reload();
                }
            },function(response){
                alert(response);
            });
    }

    function ChangeContent(changer,options) {
        var _self = this;
        this.options = {};
        this.data = {};
        this.setSettings = function() {
            this.options = {
                controller: options.controller || 'editor',
                rule: options.rule || '',
                confirm: options.confirm || '1',
                confirmMessage: options.confirmMessage || ''
            };
            var content = $(changer).parents().closest('.content-item-block').attr('data-content');
            this.data = {};
            this.data['content_id'] = content;

            if(this.options.rule == 'changeContentSize') {
                this.data['changingValue'] = $(changer).val();
            }
            if(this.options.rule == 'changeContentPosition') {
                this.data['changingValue'] = $(changer).val();
            }
            if(this.options.rule == 'changeContentText') {
                this.data['changingValue'] = $(changer).val();
            }
            if(this.options.rule == 'saveContentText') {
                this.data['changingValue'] = CKEDITOR.instances['content_'+content].getData();
            }
        };
        this.actionController = function() {
            var data = JSON.stringify(this.data);
            xhr({controller:this.options.controller,rule:this.options.rule,htmlResponse:false,data:data},
                function(response) {
                    if (response == true) {
                        if (_self.options.confirm == '1') {
                            if (window.confirm(_self.options.confirmMessage)) {
                                location.reload();
                            }
                        } else {
                            alert(_self.options.confirmMessage);
                        }
                    }
                },function(response){
                    alert(response);
                });
        };
        this.setSettings();
        this.actionController();
    }

    $.fn.extend({
        contentController:function(options) {
            options = options || {};
            var changer = $(this);
            return new ChangeContent(changer,options);
        }
    });

    $(document).ready(function() {
        $('select').material_select();

        $('.contentDelete').click(function() {
            $(this).contentController({
                rule:'deleteContent',
                confirmMessage:'Вы действительно хотите удалить этот блок'
            });
        });

        $('.contentSize').change(function() {
            $(this).contentController({
                rule:'changeContentSize',
                confirmMessage:'Вы действительно хотите изменить размер этого блока'
            });
        });

        $('.contentPosition').change(function() {
            $(this).contentController({
                rule:'changeContentPosition',
                confirmMessage:'Вы действительно хотите изменить позицию этого блока'
            });
        });
        $('.contentSave').click(function() {
            $(this).contentController({
                rule:'saveContentText',
                confirm:'0',
                confirmMessage:'Данные успесшно сохранены'
            });
        });
    });
}