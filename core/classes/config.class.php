<?php
class config
{
    private static
        $messages,
        $dbConfigs,
        $instance;

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    private function __construct()
    {
        mb_internal_encoding("UTF-8");
        self::$dbConfigs = self::getDbConfigs();
        self::$messages = self::getMessages();
        setlocale(LC_ALL, "ru_RU.UTF-8");
        return true;
    }

    public static function getMessage($key)
    {
        return self::$messages[$key];
    }

    public static function getMessages()
    {
        $messages = Array();

        foreach ($messages as $key => $value) {
            $messages[$key] = stripslashes($value);
        }

        return $messages;
    }

    public function __get($name)
    {
        return self::$dbConfigs[$name];
    }

    public static function getDbConfigs()
    {
        $configs = array(
            'db_host' => '185.28.20.9',
            'db_base' => 'u887903156_avia',
            'db_user' => 'u887903156_avia',
            'db_pass' => '055878349',
        );

        foreach ($configs as $key => $value) {
            $configs[$key] = stripslashes($value);
        }
        return $configs;
    }

    public function run(){
        $this->getConstants();
        spl_autoload_register(function ($class_name) {
            if(strpos($class_name, SMARTY) !== false) {
                if(strpos($class_name, SMARTY_INTERNAL) !== false) {
                    require_once SMARTY_PLUGINS_PATH.strtolower($class_name) . PHP;
                } elseif($class_name = SMARTY) {
                    require_once SMARTY_CLASS_PATH.$class_name.CLASS_PHP;
                }
            } elseif(strpos($class_name, PLUGIN) !== false) {
                require_once PLUGINS_PATH.$class_name.INDEX_PHP;
            } elseif(strpos($class_name, MODULE) !== false) {
                require_once MODULES_PATH.$class_name.INDEX_PHP;
            } else {
                require_once CORE_CLASSES_PATH.$class_name.CLASS_PHP;
            }
        });
        page::getInstance()->printPage();
    }

    protected function getConstants()
    {
        define("PATH", '/vagrant/web/Aviamayak');
        define("SITE", 'http://'.$_SERVER['HTTP_HOST'].'/Aviamayak/');
        define('LANGUAGE', 'RU');
        define("PLUGIN", 'plugin_');
        define("MODULE", 'module_');
        define("COMPONENT", '_Component');
        define("INDEX_PHP", '/index.php');
        define("INDEX_JSON", '/settings/index.json');
        define("ADMIN_JSON", '/settings/admin.json');
        define("INDEX_JS", '/js/index.js');
        define("INDEX_CSS", '/css/index.css');
        define("INDEX_TPL", '/templates/index.tpl');
        define("TEMPLATES", '/templates/');
        define("CLASS_PHP", '.class.php');
        define("PHP", '.php');
        define("JSON", '.json');
        define("MIN_CSS", '.min.css');
        define("MIN_JS", '.min.js');
        define("GLOB_CSS", 'includes/assets/css/*.css');
        define("CSS", '/css/*.css');
        define("GLOB_JS", 'includes/assets/js/*.js');
        define("JS", '/js/*.js');
        define("TPL", '.tpl');
        define("JS_FILES", '/js/');
        define("CSS_FILES", '/css/');
        define("_JS", '.js');
        define("_CSS", '.css');
        define("XHR", 'xhr_');
        define("CSS_SEPARATOR", '');
        define("JS_SEPARATOR", '');
        define("SMARTY", 'Smarty');
        define("SMARTY_INTERNAL", 'Smarty_Internal');
        define("SMARTY_PLUGINS_PATH", PATH.'/includes/smarty/libs/sysPlugins/');
        define("SMARTY_CLASS_PATH", PATH.'/includes/smarty/libs/');
        define("CACHE_DIR", PATH.'/includes/cache/');
        define("PLUGINS_PATH", PATH.'/plugins/');
        define("MODULES_PATH", PATH.'/modules/');
        define("CORE_CLASSES_PATH", PATH.'/core/classes/');
        define("COMPONENTS_PATH", PATH.'/components/');
        define("MIN_CSS_PATH", 'includes/assets/css/min/');
        define("MIN_JS_PATH", 'includes/assets/js/min/');
        define("XML_REQUEST", 'xmlhttprequest');
        define("FILE_WRITE_ERROR", 'Unable to open file');
        define("FILE_OVERWRITE_ERROR", 'Unable to overwrite file');
        define("GET_INSTANCE","getInstance");
        define("MODULE_FUNC","printModule");
        define("GET_DATA_FUNC","getData");
        define("MODULE_REDACT_FUNC","redactModule");
        define("GET_PLUGIN","getPlugin");
        define("CREATE_FORM","createForm");
        define("GET_PASSENGERS", 'getPassengers');
        define("MAIN","main");
        define("SERVICES","services");
        define("ACTIVE_TICKETS","tickets-flight");
        define("SETTINGS_PATH","/settings/");
        define("ADMIN_IDENTIFIER","_admin");
        define("JSON_IDENTIFIER","{");
        define("PREFIX","_");
        define("CK_EDITOR_PATH", PATH.'/includes/ckeditor/');
        define("CK_EDITOR_LINK", SITE.'includes/ckeditor/');
        define("CK_EDITOR_JS", 'ckeditor.js');
        define("RESULT_TRUE", 'true');
        define("RESULT_FALSE", 'false');
        define("EMPTY_STRING", '');
        define("DATA_MYSQL", 'mysql');
        define("DATA_JSON", 'json');
        define("TICKETS_FLIGHT", 'tickets-flight');
        define("TICKETS_CHARTER", 'tickets-charter');
        define("TICKETS_HOTEL", 'tickets-hotel');
        define("TICKETS_TOUR", 'tickets-tour');
        define("TICKETS_TRAIN", 'tickets-train');
        define("TICKETS_CAR", 'tickets-car');
        define("TICKETS_INSURANCE", 'tickets-insurance');
        define("PASSENGERS_CHARTER", 'passengers-charter');
        define("PASSENGERS_FLIGHT", 'passengers-flight');
    }
}