<div class="plugin_settings">
    <div class="position_settings settings_type">
        <div class="input-field col s12">
            <select class="contentPosition browser-default">
                <option value="" disabled selected>{$editor.position.title}</option>
                {foreach from=$editor.position.items item=positions}
                    <option value="{$positions}">{$positions}</option>
                {/foreach}
            </select>
        </div>
    </div>
    <div class="size_settings settings_type">
        <div class="input-field col s12">
            <select class="contentSize browser-default">
                <option value="" disabled selected>{$editor.size.title}</option>
                {foreach from=$editor.size.items item=size}
                    <option value="{$size.value}">{$size.title}</option>
                {/foreach}
            </select>
        </div>
    </div>
    <div class="delete_settings settings_type">
        <a class="contentDelete btn-floating btn-large waves-effect waves-light red">
            <i class="material-icons fa fa-trash"></i>
        </a>
    </div>
    <div class="save_settings settings_type">
        <a class="contentSave btn-floating btn-large waves-effect waves-light">
            <i class="material-icons fa fa-save"></i>
        </a>
    </div>
</div>