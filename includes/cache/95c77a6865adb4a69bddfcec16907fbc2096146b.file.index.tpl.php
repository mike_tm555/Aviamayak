<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-12-25 19:39:59
         compiled from "/vagrant/web/Aviamayak/modules/module_news/templates/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:9075199615678e42bf074a5-29418413%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '95c77a6865adb4a69bddfcec16907fbc2096146b' => 
    array (
      0 => '/vagrant/web/Aviamayak/modules/module_news/templates/index.tpl',
      1 => 1451072281,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '9075199615678e42bf074a5-29418413',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5678e42c106923_12643408',
  'variables' => 
  array (
    'page' => 0,
    'news' => 0,
    'settings' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5678e42c106923_12643408')) {function content_5678e42c106923_12643408($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_truncate')) include '/vagrant/web/Aviamayak/includes/smarty/libs/plugins/modifier.truncate.php';
?><div class="module_news">
    <?php if ($_smarty_tpl->tpl_vars['page']->value===false) {?>
        <div class="news_slider">
            <span class="news_slider_button news_left_button"><i class="fa fa-angle-left"></i></span>
            <span class="news_slider_button news_right_button"><i class="fa fa-angle-right"></i></span>
            
            <div class="news_slider_block row">
                <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['news']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
                    <div class="news_slider_item">
                        <div class="card large">
                            <div class="card-image">
                                <img src="/Aviamayak<?php echo $_smarty_tpl->tpl_vars['news']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['data']['image'];?>
">
                            <span class="card-title">
                                <?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['news']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['title'],30,"...",true);?>

                            </span>
                            </div>
                            <div class="card-content">
                                <p><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['news']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['text'],200,"...",true);?>
</p>
                            </div>
                            <div class="card-action">
                                <a class="waves-effect waves-light btn"><?php echo $_smarty_tpl->tpl_vars['settings']->value['newsMore'];?>
</a>
                            </div>
                        </div>
                    </div>
                <?php endfor; endif; ?>
            </div>
        </div>
    <?php } else { ?>

    <?php }?>
</div><?php }} ?>
