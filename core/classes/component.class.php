<?php
class component {

    protected
        $redact,
        $component,
        $page,
        $jsMinFile,
        $cssMinFile,
        $settings,
        $meta,
        $blocks,
        $modules,
        $plugins,
        $view,
        $cache,
        $externalScripts,
        $externalStyles;

    public function __construct($redact=false,$component,$page)
    {
        $this->redact = $redact;
        $this->component = $component;
        $this->page = $page;
        if($this->redact) {
            $this->jsMinFile = MIN_JS_PATH . $this->component.PREFIX.$this->page.ADMIN_IDENTIFIER.MIN_JS;
            $this->cssMinFile = MIN_CSS_PATH . $this->component.PREFIX.$this->page.ADMIN_IDENTIFIER.MIN_CSS;
            $this->settings  = json_decode(file_get_contents(COMPONENTS_PATH . $this->component . ADMIN_JSON), true);
        } else {
            $this->jsMinFile = MIN_JS_PATH . $this->component.PREFIX.$this->page.MIN_JS;
            $this->cssMinFile = MIN_CSS_PATH . $this->component.PREFIX.$this->page.MIN_CSS;
            $this->settings = json_decode(file_get_contents(COMPONENTS_PATH . $this->component . INDEX_JSON), true);
        }
        $this->meta = $this->settings['meta'];
        $this->blocks = $this->settings['blocks'];
        $this->modules = $this->settings['modules'];
        $this->plugins = $this->settings['plugins'];
        $this->view = $this->settings['view'];
        $this->cache = page::getInstance()->cache;
        $this->externalScripts = $this->settings['externalScripts'];
        $this->externalStyles = $this->settings['externalStyles'];
    }

    public function printComponent()
    {
        if (!page::getInstance()->isAjax) {
            echo "<!DOCTYPE html>";
            echo "<html>";
            echo "<head>";
            $this->printHead();
            echo "</head>";
            echo "<body>";
            $this->printBody();
            echo "</body>";
            echo "</html>";
        } else {
            $this->printAjax();
        }
    }

    protected function printHead()
    {
        echo '<title>'.htmlspecialchars($this->meta['title']).'</title>';
        echo '<meta http-equiv="Content-type" content="text/html; charset=utf-8" />';
        echo '<meta name="keywords" content="'.htmlspecialchars($this->meta['keywords']).'" />';
        echo '<meta name="description" content="'.htmlspecialchars($this->meta['description']).'"/>';
        echo '<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">';
        echo "<link rel='stylesheet' type='text/css' href='".$this->printStyles($this->cssMinFile)."'>";
        if(page::getInstance()->redact) {
            echo "<script type='text/javascript' src='".CK_EDITOR_LINK.CK_EDITOR_JS."'></script>";
        }
    }

    protected function  printBody()
    {
        foreach ($this->blocks as  $value) {
            echo "<div class='".$value."_block' >";
            echo "<div class='". $value."_inner_block'>";
            $this->print_Modules($this->view,$value);
            echo "</div>";
            echo "</div>";
        }
        echo "<script type='text/javascript' src='".$this->printScripts($this->jsMinFile)."'></script>";
    }


    protected function getStyles($cssFiles=null)
    {
        if($cssFiles != null) {
            $css = CSS_SEPARATOR;
            foreach ($cssFiles as $value) {
                $css .= file_get_contents(PATH.$value);
            }
        } else {
            $css = "@component:'" . $this->component . "';";
            $css .= "@page:'" . $this->page . "';";
            $css .= "@redact:'" . $this->redact . "';";
            foreach (glob(GLOB_CSS) as $file) {
                $css .= file_get_contents($file);
            }
            foreach ($this->externalStyles as $file) {
                $css .= file_get_contents($file);
            }
            foreach (glob(COMPONENTS_PATH . $this->component . CSS) as $file) {
                $css .= file_get_contents($file);
            }
            foreach ($this->modules as $value) {
                foreach (glob(MODULES_PATH . MODULE . $value . CSS) as $file) {
                    $css .= file_get_contents($file);
                }
            }
            foreach ($this->plugins as $value) {
                foreach (glob(PLUGINS_PATH . PLUGIN . $value . CSS) as $file) {
                    $css .= file_get_contents($file);
                }
            }
        }
        return $css;
//        return cssMin::getInstance()->run($css);
    }

    protected function printStyles($cssMinFile)
    {
        if (!file_exists($cssMinFile)) {
            $Css = fopen($cssMinFile, "w") or die(FILE_WRITE_ERROR);
            fwrite($Css, $this->getStyles());
            fclose($Css);
        } else {
            if ($this->cache) {
                $Css = fopen($cssMinFile, "w+") or die(FILE_OVERWRITE_ERROR);
                fwrite($Css, $this->getStyles());
                fclose($Css);
            }
        }
        return SITE.$cssMinFile;
    }

    public function print_Modules($view,$position)
    {
        foreach ($view[$position] as $value) {
            $module = MODULE . $value;
            $getInstance = GET_INSTANCE;
            $moduleFunc = MODULE_FUNC;
            $module::$getInstance()->$moduleFunc();
        }
    }

    protected function getScripts($scriptFiles=null)
    {
        if($scriptFiles != null) {
            $script = JS_SEPARATOR;
            foreach ($scriptFiles as $value) {
                $script .= file_get_contents(PATH.$value);
            }
        } else {
            $script  = "__COMPONENT__='" . $this->component . "';";
            $script .= "__PAGE__='" . $this->page . "';";
            $script .= "__REDACT__='" . $this->redact . "';";
            $script .= "__PATH__='".PATH. "';";
            $script .= "__SITE__='".SITE. "';";
            foreach (glob(GLOB_JS) as $file) {
                $script .= file_get_contents($file);
            }
            foreach ($this->externalScripts as $file) {
                $script .= file_get_contents($file);
            }
            foreach (glob(COMPONENTS_PATH . $this->component . JS) as $file) {
                $script .= file_get_contents($file);
            }
            foreach ($this->modules as $value) {
                foreach (glob(MODULES_PATH . MODULE . $value . JS) as $file) {
                    $script .= file_get_contents($file);
                }
            }
            foreach ($this->plugins as $value) {
                foreach (glob(PLUGINS_PATH.PLUGIN . $value . JS) as $file) {
                    $script .= file_get_contents($file);
                }
            }
        }
        return $script;
//        return jsMin::run($script);
    }

    protected function printScripts($jsMinFile)
    {
        if (!file_exists($jsMinFile)) {
            $Js = fopen($jsMinFile, "w") or die(FILE_WRITE_ERROR);
            fwrite($Js, $this->getScripts());
            fclose($Js);
        } else {
            if($this->cache) {
                $Js = fopen($jsMinFile, "w+") or die(FILE_OVERWRITE_ERROR);
                fwrite($Js, $this->getScripts());
                fclose($Js);
            }
        }
        return SITE.$jsMinFile;
    }

    protected function printXHRStyles($cssMinFile,$cssFiles)
    {
        if (!file_exists($cssMinFile)) {
            $Css = fopen($cssMinFile, "w") or die(FILE_WRITE_ERROR);
            fwrite($Css, $this->getStyles($cssFiles));
            fclose($Css);
        } else {
            if ($this->cache) {
                $Css = fopen($cssMinFile, "w+") or die(FILE_OVERWRITE_ERROR);
                fwrite($Css, $this->getStyles($cssFiles));
                fclose($Css);
            }
        }
        return SITE.$cssMinFile;
    }

    protected function printAjax()
    {
        $rule = page::getInstance()->seoLink;
        $controller = $rule[1];
        $do = $rule[2];
        $getInstance = GET_INSTANCE;

        if(isset($_POST['html']) && isset($_POST['css']) && isset($_POST['js'])) {

            $this->cssMinFile = MIN_CSS_PATH.XHR.$controller.MIN_CSS;
            $this->jsMinFile = MIN_JS_PATH.XHR.$controller.MIN_JS;

            $response['css'] = $this->printXHRStyles($this->cssMinFile,$_POST['css']);
            $response['js'] = $this->printXHRStyles($this->jsMinFile,$_POST['js']);

            if (in_array($controller, $this->modules)) {
                $className = MODULE . $controller;
                $response['html'] = $className::$getInstance()->$do($_POST);
            } else if (in_array($controller, $this->plugins)) {
                $className = PLUGIN . $controller;
                $response['html'] = $className::$getInstance()->$do($_POST);
            }

            echo json_encode($response,true);

        } else {

            if (in_array($controller, $this->modules)) {
                $className = MODULE . $controller;
                $className::$getInstance()->$do($_POST);
            } else if (in_array($controller, $this->plugins)) {
                $className = PLUGIN . $controller;
                $className::$getInstance()->$do($_POST);
            }

        }
    }

    protected function printXHRScripts($jsMinFile,$scriptFiles)
    {
        if (!file_exists($jsMinFile)) {
            $Js = fopen($jsMinFile, "w") or die(FILE_WRITE_ERROR);
            fwrite($Js, $this->getScripts($scriptFiles));
            fclose($Js);
        } else {
            if($this->cache) {
                $Js = fopen($jsMinFile, "w+") or die(FILE_OVERWRITE_ERROR);
                fwrite($Js, $this->getScripts($scriptFiles));
                fclose($Js);
            }
        }
        return SITE.$jsMinFile;
    }
}