<?php
class module_travelReservation extends  module
{
    protected function getDestinations($dataType,$dataAddress,$condition=false)
    {
        $data = null;
        switch($dataType)
        {
            case DATA_MYSQL:
                if($condition) {
                    $data = db::getInstance()->get_table($dataAddress, $condition);
                } else {
                    $data = db::getInstance()->get_table($dataAddress);
                }
                break;

            case DATA_JSON;
                $data = json::getInstance()->getJsonInternal($dataAddress);
                break;
        }
        return json_encode($data);
    }

    protected function getPassengers_charter()
    {

    }

    protected function getData()
    {
        $this->data['template'] = $this->tickets;
        $this->data['settings'] = $this->getModuleSettings($this->tickets);

        $getPassengers = GET_PASSENGERS;

        switch($this->tickets) {
            case TICKETS_FLIGHT:
                $this->data['destinations'] = $this->getDestinations(DATA_MYSQL,"Destinations_flight");
                $this->data['passengers-flight'] = plugin_passengers::getInstance()->$getPassengers(PASSENGERS_FLIGHT);
                break;

            case TICKETS_CHARTER:
                $this->data['destinations'] = $this->getDestinations(DATA_MYSQL,"Destinations_charter");
                $this->data['passengers-charter'] = plugin_passengers::getInstance()->$getPassengers(PASSENGERS_CHARTER);
                break;

            case TICKETS_HOTEL:

                break;

            case TICKETS_TOUR:

                break;

            case TICKETS_CAR:

                break;

            case TICKETS_TRAIN:

                break;

            case TICKETS_INSURANCE:

                break;
        }
    }
}