<?php
include_once('../core/classes/config.class.php');
include_once('../core/classes/db.class.php');


$menu = db::getInstance()->get_table('category');

$return = array();
foreach ($menu as $value) {
    if ($value['parrent_id'] == '0') {
        $underMenu = array();
        foreach ($menu as $underValue) {
            if ($underValue['parrent_id'] == $value['id']) {
                $underMenu[] = $underValue;
            }
        }


        $value['undervalue'] = $underMenu;

        $return[] = $value;
    }
}


echo "<div>";

foreach($return as $key => $value)
{
    echo "<ul>".$value['title'].'<hr>';
        foreach($value['undervalue'] as $undervalue)
        {
                echo "<li style='padding-left: 10px;'><a href='/admin/add.php?id=".$undervalue['id']." '> ".$undervalue['title']."</a></li>";
        }
    echo "</ul><hr>";
}



echo "<div style='clear: both'></div></div>";
