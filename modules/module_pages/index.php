<?php
class module_pages extends  module
{
    protected
        $table = 'Pages',
        $subTable = 'Contents',
        $page,
        $activePage;

    public function getSubPages() {
        $subPages = db::getInstance()->get_table($this->table,'parent_id = "'.$this->data['page']['id'].'"');
        foreach($subPages as $key => $value){
            db::getInstance()->orderBy('position');
            $subPages[$key]['description'] = db::getInstance()->get_table($this->subTable,'page_id="'.$value['id'].'"');
        }
        db::getInstance()->resetConditions();
        return $subPages;
    }

    public function getPage(){
        $activePage = db::getInstance()->get_fields($this->table,'seoLink="'.$this->page.'"');
        $activePage = $this->dataDecoder($activePage);
        return $activePage;
    }

    protected function getData()
    {
        $this->page = page::getInstance()->page;
        $this->data['page'] = $this->getPage();
        $this->data['subPages'] = $this->getSubPages();
        if (page::getInstance()->redact) {
            $getPlugin = GET_PLUGIN;
            $this->data['editor'] = plugin_editor::getInstance()->$getPlugin();
            $this->data['settings'] = plugin_editor::getInstance()->$getPlugin('settings');
        }
    }
}