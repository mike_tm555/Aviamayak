$(document).ready(function(){
    var news_slider = $('.news_slider_block');
    $(news_slider).owlCarousel({
        items:3,
        itemsDesktop : [1450, 3],
        stopOnHover:true,
        autoPlay : true,
        lazyLoad: true
    });
    $(".news_left_button").click(function () {
        news_slider.trigger('owl.next');
    });
    $(".news_right_button").click(function () {
        news_slider.trigger('owl.prev');
    });
});
