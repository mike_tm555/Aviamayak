<div class="module_pages">
    <div class="page-header">
        <div class="page-header-content">

        </div>
        <div class="page-header-menu row">
            <div class="col s12">
                <ul class="tabs">
                    {section name=i loop=$subPages}
                        <li class="tab col s3">
                            <a class="tab-item" data-page="{$subPages[i].id}" href="#{$subPages[i].seoLink}">{$subPages[i].title}</a>
                        </li>
                    {/section}
                </ul>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="page-content">
            <div class="page-content-inner">
                {section name=i loop=$subPages}
                    <div id="{$subPages[i].seoLink}" class="col s12">
                        {if $subPages[i].description && $subPages[i].description != ''}
                        {foreach from=$subPages[i].description item=content}
                            <div class="content-item-block {$content.class}" data-content="{$content.id}">
                            <div id="content_{$content.id}" class="content-item" {if $redact}contenteditable="true"{/if} data-content="{$content.id}">
                                {$content.content}
                            </div>
                            {if $redact}
                                {$settings}
                            {/if}
                                </div>
                        {/foreach}
                        {/if}
                    </div>
                {/section}
            </div>
        </div>
    </div>
    {if $redact}
       {$editor}
    {/if}
    <div class="page-footer"></div>
</div>