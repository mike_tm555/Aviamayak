<?php
class plugin_user extends plugin
{
    protected
        $user_default = 'user_default',
        $user_logged = 'user_logged';

    protected function userDefault()
    {
        $data['settings'] = $this->getPluginSettings($this->user_default);
        return $data;
    }

    protected function userLogged()
    {
        $data['settings'] = $this->getPluginSettings($this->user_logged);
        return $data;
    }

    protected function user()
    {
        $data['settings'] = $this->getPluginSettings();
        return $data;
    }

    protected function getData()
    {
        if($this->template) {
            switch($this->template)
            {
                case $this->user_default:
                    $Form = CREATE_FORM;
                    $this->data['user'] = $this->userDefault();
                    $this->data['loginForm'] = plugin_forms::getInstance()->$Form('loginForm');
                    $this->data['registrationForm'] = plugin_forms::getInstance()->$Form('registrationForm');
                    $this->data['remindForm'] = plugin_forms::getInstance()->$Form('remindForm');
                    break;
                case $this->user_logged:
                    $this->data['user'] = $this->userLogged();
                    break;
            }
        } else {
            $this->data['user'] = $this->user();
        }
    }
}