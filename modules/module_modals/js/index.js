function openModal(modalTrigger) {
    openModalInner(modalTrigger);
    var modal = $('.module_modals');
    if(!$(modal).hasClass('active_modal')){
        $(modal).addClass('active_modal');
        $(modal).attr('data-trigger',modalTrigger);
        $('body').addClass('remove_scroll');
        modalLoadOpen();
        setTimeout(modalLoadClose,1000);
    } else {
        if($(modal).attr('data-trigger') == modalTrigger) {
            $(modal).removeClass('active_modal');
            $(modal).removeAttr('data-trigger');
            $('body').removeClass('remove_scroll');
        } else {
            $(modal).attr('data-trigger',modalTrigger);
            $('body').addClass('remove_scroll');
            modalLoadOpen();
            setTimeout(modalLoadClose,1000);
        }
    }
}
function openModalInner(modalTrigger) {
    var modalInner = $('.modal_inner');
    var modalType = $('#'+modalTrigger+'_modal');
    $(modalInner).fadeOut(50);
    $(modalType).fadeIn(50);
}
function closeModalInner(modalTrigger) {
    var modalInner = $('.modal_inner');
    var modalType = $('#'+modalTrigger+'_modal');
    $(modalInner).fadeOut(50);
    $(modalType).fadeOut(50);
}
function modalLoadOpen() {
    var modalLoad = $('.modal_load');
    $(modalLoad).fadeIn(50);
}
function modalLoadClose() {
    var modalLoad = $('.modal_load');
    $(modalLoad).fadeOut(50);
}