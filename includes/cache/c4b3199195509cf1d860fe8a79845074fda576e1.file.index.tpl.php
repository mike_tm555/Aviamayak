<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-01-03 19:30:12
         compiled from "/vagrant/web/Aviamayak/modules/module_navigation/templates/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:6713592195678e429f11cf7-38729609%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c4b3199195509cf1d860fe8a79845074fda576e1' => 
    array (
      0 => '/vagrant/web/Aviamayak/modules/module_navigation/templates/index.tpl',
      1 => 1451849409,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6713592195678e429f11cf7-38729609',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5678e42a0ec199_35984868',
  'variables' => 
  array (
    'navMenu' => 0,
    'menuItem' => 0,
    '($_smarty_tpl->tpl_vars[\'menuItem\']->value[\'pluginData\'])' => 0,
    'listItem' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5678e42a0ec199_35984868')) {function content_5678e42a0ec199_35984868($_smarty_tpl) {?><div class="module_navigation">
    <div class="nav-prev">
    <?php  $_smarty_tpl->tpl_vars['menuItem'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['menuItem']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['navMenu']->value['preNav']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['menuItem']->key => $_smarty_tpl->tpl_vars['menuItem']->value) {
$_smarty_tpl->tpl_vars['menuItem']->_loop = true;
?>
    <a <?php if ($_smarty_tpl->tpl_vars['menuItem']->value['class']!=false) {?>class="<?php echo $_smarty_tpl->tpl_vars['menuItem']->value['class'];?>
"<?php }?>
       <?php if ($_smarty_tpl->tpl_vars['menuItem']->value['seoLink']!=false) {?>href="<?php echo $_smarty_tpl->tpl_vars['menuItem']->value['seoLink'];?>
"<?php }?>
       <?php if ($_smarty_tpl->tpl_vars['menuItem']->value['image']!=false) {?>style="<?php echo $_smarty_tpl->tpl_vars['menuItem']->value['image'];?>
"<?php }?>>
        <span class="inner" <?php if ($_smarty_tpl->tpl_vars['menuItem']->value['action']!=false) {?>onclick="<?php echo $_smarty_tpl->tpl_vars['menuItem']->value['action'];?>
"<?php }?>>
       <?php if ($_smarty_tpl->tpl_vars['menuItem']->value['icon']!=false) {?><i class="<?php echo $_smarty_tpl->tpl_vars['menuItem']->value['icon'];?>
"></i><?php }?>
       <?php if ($_smarty_tpl->tpl_vars['menuItem']->value['title']!=false) {
echo $_smarty_tpl->tpl_vars['menuItem']->value['title'];
}?>
        </span>
       <?php if ($_smarty_tpl->tpl_vars['menuItem']->value['pluginData']!=false) {
echo $_smarty_tpl->tpl_vars[($_smarty_tpl->tpl_vars['menuItem']->value['pluginData'])]->value;
}?>
    </a>
    <?php } ?>
        </div>
    <nav>
        <div class="nav-wrapper">
            <?php  $_smarty_tpl->tpl_vars['menuItem'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['menuItem']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['navMenu']->value['nav']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['menuItem']->key => $_smarty_tpl->tpl_vars['menuItem']->value) {
$_smarty_tpl->tpl_vars['menuItem']->_loop = true;
?>
                <?php if ($_smarty_tpl->tpl_vars['menuItem']->value['list']==false) {?>
                <a <?php if ($_smarty_tpl->tpl_vars['menuItem']->value['class']!=false) {?>class="<?php echo $_smarty_tpl->tpl_vars['menuItem']->value['class'];?>
"<?php }?>
                   <?php if ($_smarty_tpl->tpl_vars['menuItem']->value['seoLink']!=false) {?>href="<?php echo $_smarty_tpl->tpl_vars['menuItem']->value['seoLink'];?>
"<?php }?>
                   <?php if ($_smarty_tpl->tpl_vars['menuItem']->value['image']!=false) {?>style="<?php echo $_smarty_tpl->tpl_vars['menuItem']->value['image'];?>
"<?php }?>
                   <?php if ($_smarty_tpl->tpl_vars['menuItem']->value['action']!=false) {?>data-activates="<?php echo $_smarty_tpl->tpl_vars['menuItem']->value['action'];?>
"<?php }?>>
                   <?php if ($_smarty_tpl->tpl_vars['menuItem']->value['icon']!=false) {?><i class="<?php echo $_smarty_tpl->tpl_vars['menuItem']->value['icon'];?>
"></i><?php }?>
                   <?php if ($_smarty_tpl->tpl_vars['menuItem']->value['title']!=false) {
echo $_smarty_tpl->tpl_vars['menuItem']->value['title'];
}?>
                </a>
                <?php } else { ?>
                    <ul class="<?php echo $_smarty_tpl->tpl_vars['menuItem']->value['class'];?>
" id="<?php echo $_smarty_tpl->tpl_vars['menuItem']->value['id'];?>
">
                        <?php  $_smarty_tpl->tpl_vars['listItem'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['listItem']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['menuItem']->value['items']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['listItem']->key => $_smarty_tpl->tpl_vars['listItem']->value) {
$_smarty_tpl->tpl_vars['listItem']->_loop = true;
?>
                        <li <?php if ($_smarty_tpl->tpl_vars['listItem']->value['class']!=false) {?>class="<?php echo $_smarty_tpl->tpl_vars['listItem']->value['class'];?>
"<?php }?>>
                            <a <?php if ($_smarty_tpl->tpl_vars['listItem']->value['image']!=false) {?>style="<?php echo $_smarty_tpl->tpl_vars['listItem']->value['image'];?>
"<?php }?>
                               <?php if ($_smarty_tpl->tpl_vars['listItem']->value['seoLink']!=false) {?>href="<?php echo $_smarty_tpl->tpl_vars['listItem']->value['seoLink'];?>
"<?php }?>
                               <?php if ($_smarty_tpl->tpl_vars['listItem']->value['action']!=false) {?>data-activates="<?php echo $_smarty_tpl->tpl_vars['listItem']->value['action'];?>
"<?php }?>>
                               <?php if ($_smarty_tpl->tpl_vars['listItem']->value['icon']!=false) {?><i class="<?php echo $_smarty_tpl->tpl_vars['listItem']->value['icon'];?>
"></i><?php }?>
                               <?php if ($_smarty_tpl->tpl_vars['listItem']->value['title']!=false) {
echo $_smarty_tpl->tpl_vars['listItem']->value['title'];
}?>
                            </a>
                        </li>
                        <?php } ?>
                    </ul>
                <?php }?>
            <?php } ?>
        </div>
    </nav>
    <div class="nav-after">
        <?php  $_smarty_tpl->tpl_vars['menuItem'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['menuItem']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['navMenu']->value['afterNav']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['menuItem']->key => $_smarty_tpl->tpl_vars['menuItem']->value) {
$_smarty_tpl->tpl_vars['menuItem']->_loop = true;
?>
            <a id="<?php echo $_smarty_tpl->tpl_vars['menuItem']->value['name'];?>
"
               <?php if ($_smarty_tpl->tpl_vars['menuItem']->value['class']!=false) {?>class="<?php echo $_smarty_tpl->tpl_vars['menuItem']->value['class'];?>
"<?php }?>
               <?php if ($_smarty_tpl->tpl_vars['menuItem']->value['seoLink']!=false) {?>href="<?php echo $_smarty_tpl->tpl_vars['menuItem']->value['seoLink'];?>
"<?php }?>
               <?php if ($_smarty_tpl->tpl_vars['menuItem']->value['image']!=false) {?>style="<?php echo $_smarty_tpl->tpl_vars['menuItem']->value['image'];?>
"<?php }?>>
        <span class="inner" <?php if ($_smarty_tpl->tpl_vars['menuItem']->value['action']!=false) {?>onclick="<?php echo $_smarty_tpl->tpl_vars['menuItem']->value['action'];?>
"<?php }?>>
       <?php if ($_smarty_tpl->tpl_vars['menuItem']->value['icon']!=false) {?><i class="<?php echo $_smarty_tpl->tpl_vars['menuItem']->value['icon'];?>
"></i><?php }?>
            <?php if ($_smarty_tpl->tpl_vars['menuItem']->value['title']!=false) {
echo $_smarty_tpl->tpl_vars['menuItem']->value['title'];
}?>
        </span>
                <?php if ($_smarty_tpl->tpl_vars['menuItem']->value['pluginData']!=false) {
echo $_smarty_tpl->tpl_vars[($_smarty_tpl->tpl_vars['menuItem']->value['pluginData'])]->value;
}?>
            </a>
        <?php } ?>
    </div>
</div><?php }} ?>
