function xhr(data, successCallback, failCallback){
    if(!__COMPONENT__){
        return false;
    }
    var _self = this;
    _self.hostName = __SITE__;
    _self.controller = data.controller || null;
    _self.rule = data.rule || null;
    _self.tagAttribute = _self.controller + '_' + _self.rule;
    _self.htmlResponse = data.htmlResponse || false;
    _self.data = data.data || null;
    if(!_self.controller || !_self.rule) return false;
    $.post(_self.hostName + __COMPONENT__ + '/' + _self.controller + '/' + _self.rule, _self.data, function(response) {
        if(response) {
            if(_self.htmlResponse) {
                var response_data = JSON.parse(response);
                //check if elements with tag data-xhr are not exist and include css and js
                if($("[data-xhr='" + _self.tagAttribute +"']").length <= 0){
                    //include css from response
                    var CssElem = document.createElement("link");
                    CssElem.rel = 'stylesheet';
                    CssElem.type = 'text/css';
                    CssElem.href = response_data.css;
                    CssElem.setAttribute("data-xhr", _self.tagAttribute);
                    document.head.appendChild(CssElem);
                    //include js from response
                    var JsElem = document.createElement("script");
                    JsElem.type = 'text/javascript';
                    JsElem.src = response_data.js;
                    JsElem.setAttribute("data-xhr", _self.tagAttribute);
                    document.body.appendChild(JsElem);
                }
                successCallback(response_data.html);
            } else {
                try {
                    response_data = JSON.parse(response);
                } catch(e) {
                    response_data = response;
                }
                successCallback(response_data);
            }
        } else {
            failCallback(response);
        }
    });
}

AVM = {

};