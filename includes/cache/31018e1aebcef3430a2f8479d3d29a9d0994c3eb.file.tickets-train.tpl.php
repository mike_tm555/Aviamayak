<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-12-30 22:46:31
         compiled from "/vagrant/web/Aviamayak/plugins/plugin_searches/templates/external/tickets-train.tpl" */ ?>
<?php /*%%SmartyHeaderCode:91345049556830307e5fc72-12301956%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '31018e1aebcef3430a2f8479d3d29a9d0994c3eb' => 
    array (
      0 => '/vagrant/web/Aviamayak/plugins/plugin_searches/templates/external/tickets-train.tpl',
      1 => 1451515586,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '91345049556830307e5fc72-12301956',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56830307ec6954_38745215',
  'variables' => 
  array (
    'destinations' => 0,
    'settings' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56830307ec6954_38745215')) {function content_56830307ec6954_38745215($_smarty_tpl) {?><div class="inputs_block" id="tickets-train">
    <div class="input_middle input_block">
        <div class="input-field col s6">
            <input id="destinationFrom" data-data='<?php echo $_smarty_tpl->tpl_vars['destinations']->value;?>
' type="text" class="input_text input autoComplete">
            <label for="destinationFrom"><?php echo $_smarty_tpl->tpl_vars['settings']->value['searchElements']['destinationFrom']['title'];?>
</label>
            <div class="output" id="destinationFrom_output">
            </div>
        </div>
    </div>
    <div class="input_middle input_block">
        <div class="input-field col s6">
            <input id="destinationTo" data-data='<?php echo $_smarty_tpl->tpl_vars['destinations']->value;?>
' type="text" class="input_text input autoComplete">
            <label for="destinationTo"><?php echo $_smarty_tpl->tpl_vars['settings']->value['searchElements']['destinationTo']['title'];?>
</label>
            <div class="output" id="destinationTo_output">
            </div>
        </div>
    </div>
    <div class="input_middle input_block">
        <div id="dateTo_block" class="input-field col s6 right_only">
            <input id="dateTo" type="text" readonly class="input_text input date">
            <label for="dateTo"><?php echo $_smarty_tpl->tpl_vars['settings']->value['searchElements']['dateTo']['title'];?>
</label>
        </div>
    </div>
</div>
<div class="button_block">
    <div class="button_inner_block">
        <button class="<?php echo $_smarty_tpl->tpl_vars['settings']->value['searchElements']['submit']['class'];?>
">
            <?php echo $_smarty_tpl->tpl_vars['settings']->value['searchElements']['submit']['title'];?>

        </button>
    </div>
</div>
<?php }} ?>
