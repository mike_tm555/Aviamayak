<div class="plugin_editor">
     <div class="fixed-action-btn">
          <a class="btn-floating btn-large red">
               <i class="large material-icons fa fa-pencil"></i>
          </a>
          <ul>
               <li>
                    <a onclick="addContent()" class="waves-effect waves-light btn-floating red">
                         <i class="material-icons fa fa-plus"></i>
                    </a>
               </li>
               <li>
                    <a class="waves-effect waves-light btn-floating green">
                         <i class="material-icons fa fa-trash"></i>
                    </a>
               </li>
          </ul>
     </div>
     <div id="modal_add_content" class="modal modal-fixed-footer">
          <div class="modal-content">
               <h5>{$editor.addNewContent.title}</h5>
               <div class="divider"></div>

          </div>
          <div class="modal-footer">
               <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">
                    <a class="waves-effect waves-light btn">
                    <i class="material-icons fa fa-save"></i>
                    </a>
               </a>
          </div>
     </div>
</div>