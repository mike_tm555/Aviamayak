<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-01-11 18:21:44
         compiled from "/vagrant/web/Aviamayak/plugins/plugin_forms/templates/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3099774385678e42c1c2957-94458824%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3ce21599cdd026f06a2f100fac15fe13738089a1' => 
    array (
      0 => '/vagrant/web/Aviamayak/plugins/plugin_forms/templates/index.tpl',
      1 => 1452536503,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3099774385678e42c1c2957-94458824',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5678e42c3dccc1_18696260',
  'variables' => 
  array (
    'form' => 0,
    'step' => 0,
    'element' => 0,
    'group_element' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5678e42c3dccc1_18696260')) {function content_5678e42c3dccc1_18696260($_smarty_tpl) {?><div class="<?php echo $_smarty_tpl->tpl_vars['form']->value['blockClass'];?>
">
    <form  class="form <?php echo $_smarty_tpl->tpl_vars['form']->value['class'];?>
"
           id="<?php echo $_smarty_tpl->tpl_vars['form']->value['name'];?>
"
            <?php if ($_smarty_tpl->tpl_vars['form']->value['action']!=false) {?>
                action="<?php echo $_smarty_tpl->tpl_vars['form']->value['action'];?>
"
            <?php }?>
           method="<?php echo $_smarty_tpl->tpl_vars['form']->value['method'];?>
"
           data-name="<?php echo $_smarty_tpl->tpl_vars['form']->value['name'];?>
"
            <?php if ($_smarty_tpl->tpl_vars['form']->value['ajax']!=false) {?>
                data-ajax="<?php echo $_smarty_tpl->tpl_vars['form']->value['ajax'];?>
"
            <?php }?>
           data-language="<?php echo $_smarty_tpl->tpl_vars['form']->value['language'];?>
">
        <?php  $_smarty_tpl->tpl_vars['step'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['step']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['form']->value['steps']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['step']->key => $_smarty_tpl->tpl_vars['step']->value) {
$_smarty_tpl->tpl_vars['step']->_loop = true;
?>
            <div class="form_step"
                 id="step_<?php echo $_smarty_tpl->tpl_vars['form']->value['name'];?>
_<?php echo $_smarty_tpl->tpl_vars['step']->value['stepID'];?>
"
                 data-controller="<?php echo $_smarty_tpl->tpl_vars['step']->value['stepController'];?>
"
                 data-action="<?php echo $_smarty_tpl->tpl_vars['step']->value['stepAction'];?>
">
                <?php  $_smarty_tpl->tpl_vars['element'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['element']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['step']->value['elements']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['element']->key => $_smarty_tpl->tpl_vars['element']->value) {
$_smarty_tpl->tpl_vars['element']->_loop = true;
?>
                    <div class="form-group form-group-label" <?php if ($_smarty_tpl->tpl_vars['element']->value['htmlType']=='input'&&$_smarty_tpl->tpl_vars['element']->value['type']=="hidden") {?>style="display:none;"<?php }?>>
                        <div class="row">
                            <div class="<?php echo $_smarty_tpl->tpl_vars['element']->value['size'];?>
">
                                <?php if ($_smarty_tpl->tpl_vars['element']->value['icon']!=false) {?>
                                    <i class="<?php echo $_smarty_tpl->tpl_vars['element']->value['icon'];?>
"></i>
                                <?php }?>
                                <?php if ($_smarty_tpl->tpl_vars['element']->value['htmlType']=="input") {?>
                                    <?php if ($_smarty_tpl->tpl_vars['element']->value['type']=="submit"||$_smarty_tpl->tpl_vars['element']->value['type']=="button") {?>
                                        <button class="<?php echo $_smarty_tpl->tpl_vars['element']->value['dataType'];?>
 <?php echo $_smarty_tpl->tpl_vars['element']->value['className'];?>
"
                                               type="<?php echo $_smarty_tpl->tpl_vars['element']->value['type'];?>
"
                                               data-step="step_<?php echo $_smarty_tpl->tpl_vars['form']->value['name'];?>
_<?php echo $_smarty_tpl->tpl_vars['step']->value['stepID'];?>
">
                                            <?php echo $_smarty_tpl->tpl_vars['element']->value['values'][0];?>

                                            </button>
                                    <?php } elseif ($_smarty_tpl->tpl_vars['element']->value['type']=="hidden") {?>
                                        <input class="<?php echo $_smarty_tpl->tpl_vars['element']->value['dataType'];?>
 <?php echo $_smarty_tpl->tpl_vars['element']->value['className'];?>
"
                                               name="<?php echo $_smarty_tpl->tpl_vars['element']->value['name'];?>
"
                                               id="<?php echo $_smarty_tpl->tpl_vars['element']->value['name'];?>
"
                                               type="<?php echo $_smarty_tpl->tpl_vars['element']->value['type'];?>
"
                                               data-step="step_<?php echo $_smarty_tpl->tpl_vars['form']->value['name'];?>
_<?php echo $_smarty_tpl->tpl_vars['step']->value['stepID'];?>
"
                                               data-rules="<?php echo $_smarty_tpl->tpl_vars['element']->value['rules'];?>
"
                                               data-display="<?php echo $_smarty_tpl->tpl_vars['element']->value['display'];?>
"
                                               data-depends="<?php echo $_smarty_tpl->tpl_vars['element']->value['depends'];?>
"
                                               value="<?php echo $_smarty_tpl->tpl_vars['element']->value['values'][0];?>
"
                                                <?php if ($_smarty_tpl->tpl_vars['element']->value['placeholder']!=false) {?>
                                                    placeholder="<?php echo $_smarty_tpl->tpl_vars['element']->value['placeholder'];?>
"
                                                <?php }?>
                                               data-dependence="<?php echo $_smarty_tpl->tpl_vars['element']->value['dependence'];?>
"/>

                                    <?php } elseif ($_smarty_tpl->tpl_vars['element']->value['type']=="radio"||$_smarty_tpl->tpl_vars['element']->value['type']=="checkbox") {?>
                                        <span class="group_inputs_title"><?php echo $_smarty_tpl->tpl_vars['element']->value['title'];?>
</span>
                                        <?php if ($_smarty_tpl->tpl_vars['element']->value['group']!=false) {?>
                                            <div class="group_elements">
                                                <?php  $_smarty_tpl->tpl_vars['group_element'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['group_element']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['element']->value['group_elements']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['group_element']->key => $_smarty_tpl->tpl_vars['group_element']->value) {
$_smarty_tpl->tpl_vars['group_element']->_loop = true;
?>
                                                    <div class="<?php echo $_smarty_tpl->tpl_vars['group_element']->value['size'];?>
">
                                                        <label for="<?php echo $_smarty_tpl->tpl_vars['group_element']->value['id'];?>
">
                                                            <?php echo $_smarty_tpl->tpl_vars['group_element']->value['label'];?>

                                                            <input class="<?php echo $_smarty_tpl->tpl_vars['group_element']->value['dataType'];?>
 <?php echo $_smarty_tpl->tpl_vars['group_element']->value['className'];?>
"
                                                                   name="<?php echo $_smarty_tpl->tpl_vars['group_element']->value['name'];?>
"
                                                                   id="<?php echo $_smarty_tpl->tpl_vars['group_element']->value['id'];?>
"
                                                                   type="<?php echo $_smarty_tpl->tpl_vars['element']->value['type'];?>
"
                                                                   value="<?php echo $_smarty_tpl->tpl_vars['group_element']->value['values'][0];?>
"
                                                                   data-rules="<?php echo $_smarty_tpl->tpl_vars['group_element']->value['rules'];?>
"
                                                                   data-display="<?php echo $_smarty_tpl->tpl_vars['group_element']->value['display'];?>
"
                                                                   data-depends="<?php echo $_smarty_tpl->tpl_vars['group_element']->value['depends'];?>
"
                                                                    <?php if ($_smarty_tpl->tpl_vars['group_element']->value['placeholder']!=false) {?>
                                                                        placeholder="<?php echo $_smarty_tpl->tpl_vars['group_element']->value['placeholder'];?>
"
                                                                    <?php }?>
                                                                   data-dependence="<?php echo $_smarty_tpl->tpl_vars['group_element']->value['dependence'];?>
"/>
                                                            <span class="circle"></span>
                                                            <span class="circle-check"></span>
                                                        </label>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        <?php }?>
                                    <?php } else { ?>
                                        <input class="<?php echo $_smarty_tpl->tpl_vars['element']->value['dataType'];?>
 <?php echo $_smarty_tpl->tpl_vars['element']->value['className'];?>
"
                                               name="<?php echo $_smarty_tpl->tpl_vars['element']->value['name'];?>
"
                                               id="<?php echo $_smarty_tpl->tpl_vars['element']->value['name'];?>
"
                                               type="<?php echo $_smarty_tpl->tpl_vars['element']->value['type'];?>
"
                                               data-step="step_<?php echo $_smarty_tpl->tpl_vars['form']->value['name'];?>
_<?php echo $_smarty_tpl->tpl_vars['step']->value['stepID'];?>
"
                                               data-rules="<?php echo $_smarty_tpl->tpl_vars['element']->value['rules'];?>
"
                                               data-display="<?php echo $_smarty_tpl->tpl_vars['element']->value['display'];?>
"
                                               data-depends="<?php echo $_smarty_tpl->tpl_vars['element']->value['depends'];?>
"
                                                <?php if ($_smarty_tpl->tpl_vars['element']->value['placeholder']!=false) {?>
                                                    placeholder="<?php echo $_smarty_tpl->tpl_vars['element']->value['placeholder'];?>
"
                                                <?php }?>
                                               data-dependence="<?php echo $_smarty_tpl->tpl_vars['element']->value['dependence'];?>
"/>
                                    <?php }?>
                                <?php } elseif ($_smarty_tpl->tpl_vars['element']->value['htmlType']=="select") {?>
                                    <select class="<?php echo $_smarty_tpl->tpl_vars['element']->value['dataType'];?>
 <?php echo $_smarty_tpl->tpl_vars['element']->value['className'];?>
"
                                            name="<?php echo $_smarty_tpl->tpl_vars['element']->value['name'];?>
"
                                            id="<?php echo $_smarty_tpl->tpl_vars['element']->value['name'];?>
"
                                            data-step="step_<?php echo $_smarty_tpl->tpl_vars['form']->value['name'];?>
_<?php echo $_smarty_tpl->tpl_vars['step']->value['stepID'];?>
"
                                            data-rules="<?php echo $_smarty_tpl->tpl_vars['element']->value['rules'];?>
"
                                            data-display="<?php echo $_smarty_tpl->tpl_vars['element']->value['display'];?>
"
                                            data-depends="<?php echo $_smarty_tpl->tpl_vars['element']->value['depends'];?>
"
                                            data-dependence="<?php echo $_smarty_tpl->tpl_vars['element']->value['dependence'];?>
">
                                        <?php if ($_smarty_tpl->tpl_vars['element']->value['default']!=false) {?>
                                            <option value="null"><?php echo $_smarty_tpl->tpl_vars['element']->value['default'];?>
</option>
                                        <?php }?>
                                        <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['element']->value['values']) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
                                            <option value="<?php echo $_smarty_tpl->tpl_vars['element']->value['values'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['data'];?>
"><?php echo $_smarty_tpl->tpl_vars['element']->value['values'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['value'];?>
</option>
                                        <?php endfor; endif; ?>
                                    </select>
                                <?php } elseif ($_smarty_tpl->tpl_vars['element']->value['htmlType']=="textarea") {?>
                                    <textarea class="<?php echo $_smarty_tpl->tpl_vars['element']->value['dataType'];?>
 <?php echo $_smarty_tpl->tpl_vars['element']->value['className'];?>
"
                                              name="<?php echo $_smarty_tpl->tpl_vars['element']->value['name'];?>
"
                                              id="<?php echo $_smarty_tpl->tpl_vars['element']->value['name'];?>
"
                                              data-step="step_<?php echo $_smarty_tpl->tpl_vars['form']->value['name'];?>
_<?php echo $_smarty_tpl->tpl_vars['step']->value['stepID'];?>
"
                                              data-rules="<?php echo $_smarty_tpl->tpl_vars['element']->value['rules'];?>
"
                                              data-display="<?php echo $_smarty_tpl->tpl_vars['element']->value['display'];?>
"
                                              data-depends="<?php echo $_smarty_tpl->tpl_vars['element']->value['depends'];?>
"
                                            <?php if ($_smarty_tpl->tpl_vars['element']->value['placeholder']!=false) {?>
                                                placeholder="<?php echo $_smarty_tpl->tpl_vars['element']->value['placeholder'];?>
"
                                            <?php }?>
                                              data-dependence="<?php echo $_smarty_tpl->tpl_vars['element']->value['dependence'];?>
"></textarea>
                                <?php }?>
                                <?php if ($_smarty_tpl->tpl_vars['element']->value['label']!=false) {?>
                                    <label class="floating-label" for="<?php echo $_smarty_tpl->tpl_vars['element']->value['name'];?>
"><?php echo $_smarty_tpl->tpl_vars['element']->value['label'];?>
</label>
                                <?php }?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </form>
</div><?php }} ?>
