<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-12-22 05:48:26
         compiled from "/vagrant/web/Aviamayak/plugins/plugin_searches/templates/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:5724663405678e42a5b57e3-11120681%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bf1dab39bee64a4100f060813c9d07f877522c27' => 
    array (
      0 => '/vagrant/web/Aviamayak/plugins/plugin_searches/templates/index.tpl',
      1 => 1450100425,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5724663405678e42a5b57e3-11120681',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'search' => 0,
    'searchItem' => 0,
    'passengers' => 0,
    'passengerType' => 0,
    'cabinClassTypes' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5678e42a6d4c56_64225872',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5678e42a6d4c56_64225872')) {function content_5678e42a6d4c56_64225872($_smarty_tpl) {?><?php if ($_smarty_tpl->tpl_vars['search']->value['dataType']=='internal') {?>
<div class="av_search">
    <?php  $_smarty_tpl->tpl_vars['searchItem'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['searchItem']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['search']->value['searchElements']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['searchItem']->key => $_smarty_tpl->tpl_vars['searchItem']->value) {
$_smarty_tpl->tpl_vars['searchItem']->_loop = true;
?>
        <?php if ($_smarty_tpl->tpl_vars['searchItem']->value['htmlType']=='comboBox') {?>
            <div style="<?php echo $_smarty_tpl->tpl_vars['searchItem']->value['width'];?>
" class="input_div" id="<?php echo $_smarty_tpl->tpl_vars['searchItem']->value['name'];?>
_div">
                <span class="input_icon"><i class="<?php echo $_smarty_tpl->tpl_vars['searchItem']->value['icon'];?>
"></i></span>
                <input class='<?php echo $_smarty_tpl->tpl_vars['searchItem']->value['class'];?>
'
                       type='text'
                       name='<?php echo $_smarty_tpl->tpl_vars['searchItem']->value['name'];?>
'
                       id='<?php echo $_smarty_tpl->tpl_vars['searchItem']->value['name'];?>
'
                       placeholder='<?php echo $_smarty_tpl->tpl_vars['searchItem']->value['placeholder'];?>
'
                       data-htmlType='<?php echo $_smarty_tpl->tpl_vars['searchItem']->value['htmlType'];?>
'
                       data-type='<?php echo $_smarty_tpl->tpl_vars['searchItem']->value['dataType'];?>
'
                       data-name='<?php echo $_smarty_tpl->tpl_vars['searchItem']->value['dataName'];?>
'
                       data-data='<?php echo $_smarty_tpl->tpl_vars['searchItem']->value['data'];?>
'>
                <div class="output" id="<?php echo $_smarty_tpl->tpl_vars['searchItem']->value['name'];?>
_output"></div>
            </div>
        <?php } else { ?>
            <div style="<?php echo $_smarty_tpl->tpl_vars['searchItem']->value['width'];?>
"  class="input_div" id="<?php echo $_smarty_tpl->tpl_vars['searchItem']->value['name'];?>
_div">
                <span class="input_icon"><i class="<?php echo $_smarty_tpl->tpl_vars['searchItem']->value['icon'];?>
"></i></span>
                <input class='<?php echo $_smarty_tpl->tpl_vars['searchItem']->value['class'];?>
'
                       type='text'
                       name='<?php echo $_smarty_tpl->tpl_vars['searchItem']->value['name'];?>
'
                       id='<?php echo $_smarty_tpl->tpl_vars['searchItem']->value['name'];?>
'
                       placeholder='<?php echo $_smarty_tpl->tpl_vars['searchItem']->value['placeholder'];?>
'
                       data-htmlType='<?php echo $_smarty_tpl->tpl_vars['searchItem']->value['htmlType'];?>
'
                       data-type='<?php echo $_smarty_tpl->tpl_vars['searchItem']->value['dataType'];?>
'
                       data-name='<?php echo $_smarty_tpl->tpl_vars['searchItem']->value['dataName'];?>
'
                       data-data='<?php echo $_smarty_tpl->tpl_vars['searchItem']->value['data'];?>
'>
            </div>
        <?php }?>
    <?php } ?>
</div>
<div class="searchPicker_overlay">
    <div id="searchPicker">
        <?php  $_smarty_tpl->tpl_vars['passengerType'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['passengerType']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['passengers']->value['passengers']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['passengerType']->key => $_smarty_tpl->tpl_vars['passengerType']->value) {
$_smarty_tpl->tpl_vars['passengerType']->_loop = true;
?>
            <div class="passenger_block">
                <span class="passengerType"><?php echo $_smarty_tpl->tpl_vars['passengerType']->value['title'];?>
</span>
                <div id="<?php echo $_smarty_tpl->tpl_vars['passengerType']->value['id'];?>
" class="count_change">
                    <span class="minus"><i class="fa fa-minus"></i></span>
                    <span class="passengers_count"><?php echo $_smarty_tpl->tpl_vars['passengerType']->value['default'];?>
</span>
                    <span class="plus"><i class="fa fa-plus"></i></span>
                </div>
            </div>
        <?php } ?>
        <div class="cabin_class">
            <label><?php echo $_smarty_tpl->tpl_vars['passengers']->value['cabinClass']['label'];?>
</label>
            <select class="cabin_class_type" name="<?php echo $_smarty_tpl->tpl_vars['passengers']->value['cabinClass']['name'];?>
">
                <?php  $_smarty_tpl->tpl_vars['cabinClassTypes'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['cabinClassTypes']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['passengers']->value['cabinClass']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['cabinClassTypes']->key => $_smarty_tpl->tpl_vars['cabinClassTypes']->value) {
$_smarty_tpl->tpl_vars['cabinClassTypes']->_loop = true;
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['cabinClassTypes']->value['value'];?>
"><?php echo $_smarty_tpl->tpl_vars['cabinClassTypes']->value['value'];?>
</option>
                <?php } ?>
            </select>
        </div>
        <div class="passengers_result">
            <div class="cabin_class_result">
            </div>
            <div class="passengersCount_result">
            </div>
        </div>
    </div>
</div>
<div class="results_searching_block">
    <div class="results_searching_inner_block">
        <button class="<?php echo $_smarty_tpl->tpl_vars['search']->value['submit']['class'];?>
">
            <?php echo $_smarty_tpl->tpl_vars['search']->value['submit']['value'];?>

        </button>
    </div>
</div>
<?php } elseif ($_smarty_tpl->tpl_vars['search']->value['dataType']=='external') {?>
    <?php echo $_smarty_tpl->tpl_vars['search']->value['data'];?>

<?php }?><?php }} ?>
