AVM.TravelReservation.Passengers = {

    picker:function(toggler) {
        var block = $('.passengers-block');
        var toggleBlock = $(toggler).parent();
        $(toggleBlock).click(function(event) {
            $('html').one('click',function() {
                AVM.TravelReservation.Passengers.close(block);
            });

            AVM.TravelReservation.Passengers.open(block);

            event.stopPropagation();
        });
    },

    close:function(block) {
        $(block).removeClass('active-passengers');
    },

    open:function(block) {
        $(block).addClass('active-passengers');
    },

    countChange:function(type,countBlock) {
        var passengersCountBlock = $('#'+countBlock),
            minValue = $(passengersCountBlock).attr('min'),
            maxValue = $(passengersCountBlock).attr('max'),
            passengersCount = parseFloat($(passengersCountBlock).val());

        if (type == 'minus') {
            passengersCount = passengersCount - 1;
        } else {
            passengersCount = passengersCount + 1;
        }
        if(passengersCount <= maxValue && passengersCount >= minValue) {
            $(passengersCountBlock).val(passengersCount).trigger('change');
        }
    },

    getCount:function() {
        var passengersCount = 0;
        $('.passengers-input').each(function(){
            passengersCount = passengersCount + parseFloat($(this).val())
        });

        if(passengersCount > 1) {
            passengersCount =  passengersCount + ' пассажира';
        } else {
            passengersCount = passengersCount + ' пассажир';
        }
        return passengersCount;
    },

    getClassTitle:function() {
        var classToggle = $('#passenger-class-type'),
            classChecked = $(classToggle).is(":checked"),
            passengersClass;

        if(!classChecked) {
            passengersClass = $(classToggle).attr('data-checked-title')
        } else {
            passengersClass = $(classToggle).attr('data-unchecked-title');
        }
        return (passengersClass);
    },

    getClassData:function() {
        var classToggle = $('#passenger-class-type'),
            classChecked = $(classToggle).is(":checked"),
            passengersClass;

        if(!classChecked) {
            passengersClass = $(classToggle).attr('data-checked-data')
        } else {
            passengersClass = $(classToggle).attr('data-unchecked-data');
        }
        return (passengersClass);
    },

    passengersChanger:function() {
        var passengersCount = AVM.TravelReservation.Passengers.getCount();
        var passengersClass = AVM.TravelReservation.Passengers.getClassTitle();
        $('#passengersAndClasses').val(passengersCount + ',' + passengersClass);
    },

    passengersChangeHandler:function() {
        AVM.TravelReservation.Passengers.passengersChanger();

        $('.passengers-input').bind("change",function(){
            AVM.TravelReservation.Passengers.passengersChanger();
        });

        $('#passenger-class-type').on("change",function(){
            AVM.TravelReservation.Passengers.passengersChanger();
        });
    }
};

$(document).ready(function() {
    AVM.TravelReservation.Passengers.passengersChangeHandler();
});