<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-12-30 22:42:17
         compiled from "/vagrant/web/Aviamayak/plugins/plugin_searches/templates/external/tickets-tour.tpl" */ ?>
<?php /*%%SmartyHeaderCode:56568871956837cad1221f1-72362157%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '668ab1b4ac70d014862fb62082585801d4529c82' => 
    array (
      0 => '/vagrant/web/Aviamayak/plugins/plugin_searches/templates/external/tickets-tour.tpl',
      1 => 1451515333,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '56568871956837cad1221f1-72362157',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56837cad198190_27938206',
  'variables' => 
  array (
    'destinationsFrom' => 0,
    'settings' => 0,
    'destinationsTo' => 0,
    'passengers' => 0,
    'passenger' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56837cad198190_27938206')) {function content_56837cad198190_27938206($_smarty_tpl) {?><div class="inputs_block" id="tickets-tour">
    <div class="input_small input_block">
        <div class="input-field col s6">
            <input id="destinationFrom" data-data='<?php echo $_smarty_tpl->tpl_vars['destinationsFrom']->value;?>
' type="text" class="input_text input autoComplete">
            <label for="destinationFrom"><?php echo $_smarty_tpl->tpl_vars['settings']->value['searchElements']['destinationFrom']['title'];?>
</label>
            <div class="output" id="destinationFrom_output">
            </div>
        </div>
    </div>
    <div class="input_small input_block">
        <div class="input-field col s6">
            <input id="destinationTo" data-data='<?php echo $_smarty_tpl->tpl_vars['destinationsTo']->value;?>
' type="text" class="input_text input autoComplete">
            <label for="destinationTo"><?php echo $_smarty_tpl->tpl_vars['settings']->value['searchElements']['destinationTo']['title'];?>
</label>
            <div class="output" id="destinationTo_output">
            </div>
        </div>
    </div>
    <div class="input_smallest input_block">
        <div id="dateFrom_block" class="input-field col s6 right_multiple">
            <input id="dateFrom" type="text" readonly class="input_text input date">
            <label for="dateFrom"><?php echo $_smarty_tpl->tpl_vars['settings']->value['searchElements']['dateFrom']['title'];?>
</label>
        </div>
    </div>
    <div class="input_small input_block">
        <div class="input-field col s6">
            <input id="destinationTo"
                   type="text"
                   data-activates='dropdownPassengers'
                   value="<?php echo $_smarty_tpl->tpl_vars['settings']->value['searchElements']['passengers']['default'];?>
"
                   class="input_text input dropdown-button">
        </div>
        <ul id='dropdownPassengers' class='dropdown-content'>
            <?php  $_smarty_tpl->tpl_vars['passenger'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['passenger']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['passengers']->value['passengers']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['passenger']->key => $_smarty_tpl->tpl_vars['passenger']->value) {
$_smarty_tpl->tpl_vars['passenger']->_loop = true;
?>
                <li>
                    <div class="passenger_block">
                        <span class="increment-label"><?php echo $_smarty_tpl->tpl_vars['passenger']->value['value'];?>
</span>
                        <div class="increment-block">
                            <div id="dec" class="dec num-button"><i class="fa fa-minus"></i></div>
                            <input type="number"
                                   min="<?php echo $_smarty_tpl->tpl_vars['passenger']->value['min'];?>
"
                                   max="<?php echo $_smarty_tpl->tpl_vars['passenger']->value['max'];?>
"
                                   id="<?php echo $_smarty_tpl->tpl_vars['passenger']->value['id'];?>
"
                                   class="increment-input"
                                   value="<?php echo $_smarty_tpl->tpl_vars['passenger']->value['count'];?>
">
                            <div id="inc" class="inc num-button"><i class="fa fa-plus"></i></div>
                        </div>
                    </div>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>
<div class="button_block">
    <div class="button_inner_block">
        <button class="<?php echo $_smarty_tpl->tpl_vars['settings']->value['searchElements']['submit']['class'];?>
">
            <?php echo $_smarty_tpl->tpl_vars['settings']->value['searchElements']['submit']['title'];?>

        </button>
    </div>
</div>
<?php }} ?>
