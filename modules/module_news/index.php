<?php
class module_news extends module
{
    protected
        $table = 'News',
        $component,
        $seoLink;

    public function getLastNews(){
        db::getInstance()->orderBy('id', 'DESC');
        db::getInstance()->limit(30);
        $news = db::getInstance()->get_table($this->table,"parent_id != '0'");
        db::getInstance()->resetConditions();
        foreach($news as $key=> $value) {
            $news[$key] = $this->dataDecoder($value);
        }
        return $news;
    }

    protected function getData(){
        $this->component = page::getInstance()->component;
        $this->seoLink = page::getInstance()->seoLink;
        $this->data['settings'] = $this->getModuleSettings();
        if($this->component != 'news') {
            $this->data['page'] = false;
            $this->data['news'] = $this->getLastNews();
        }
    }
}