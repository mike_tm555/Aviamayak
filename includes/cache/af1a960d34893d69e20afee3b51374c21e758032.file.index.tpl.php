<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-12-22 17:49:56
         compiled from "/vagrant/web/Aviamayak/modules/module_aboutUs/templates/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:13358520555678e42c14d407-33207417%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'af1a960d34893d69e20afee3b51374c21e758032' => 
    array (
      0 => '/vagrant/web/Aviamayak/modules/module_aboutUs/templates/index.tpl',
      1 => 1450806592,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13358520555678e42c14d407-33207417',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5678e42c165345_05437407',
  'variables' => 
  array (
    'aboutUs' => 0,
    'sliderItem' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5678e42c165345_05437407')) {function content_5678e42c165345_05437407($_smarty_tpl) {?><div class="module_aboutUs">
    <div class="title">
        <span class="title_text"><?php echo $_smarty_tpl->tpl_vars['aboutUs']->value['title'];?>
</span>
        <span class="title_icon btn-floating btn-large waves-effect waves-light transparent"><i class="fa fa-arrow-down"></i></span>
    </div>
    <div class="aboutUs_slider">
        <span id="aboutUs_button_left" class="slide_button button_left"><i class="fa fa-angle-left"></i></span>
        <span id="aboutUs_button_right" class="slide_button button_right"><i class="fa fa-angle-right"></i></span>
        <div class="aboutUs_slider_inner">
        <div class="aboutUs_slider_title">
            <span class="aboutUs_slider_title_block"></span>
            <span class="aboutUs_slider_title_separator"></span>
        </div>
        <div class="aboutUs_slider_block">
            <?php  $_smarty_tpl->tpl_vars['sliderItem'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['sliderItem']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['aboutUs']->value['slider']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['sliderItem']->key => $_smarty_tpl->tpl_vars['sliderItem']->value) {
$_smarty_tpl->tpl_vars['sliderItem']->_loop = true;
?>
                <div class="aboutUs_slider_item">
                    <div class="slider_icon"><i class="<?php echo $_smarty_tpl->tpl_vars['sliderItem']->value['icon'];?>
"></i></div>
                    <div class="slider_title"><?php echo $_smarty_tpl->tpl_vars['sliderItem']->value['title'];?>
</div>
                    <div class="slider_text"><?php echo $_smarty_tpl->tpl_vars['sliderItem']->value['text'];?>
</div>
                </div>
            <?php } ?>
        </div>
    </div>
    </div>
</div><?php }} ?>
