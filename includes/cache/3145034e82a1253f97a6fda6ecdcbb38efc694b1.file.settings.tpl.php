<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-12-25 16:55:27
         compiled from "/vagrant/web/Aviamayak/plugins/plugin_editor/templates/settings.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1815196271567ca69c833fe9-95151538%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3145034e82a1253f97a6fda6ecdcbb38efc694b1' => 
    array (
      0 => '/vagrant/web/Aviamayak/plugins/plugin_editor/templates/settings.tpl',
      1 => 1451062513,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1815196271567ca69c833fe9-95151538',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_567ca69c866e89_72469847',
  'variables' => 
  array (
    'editor' => 0,
    'positions' => 0,
    'size' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_567ca69c866e89_72469847')) {function content_567ca69c866e89_72469847($_smarty_tpl) {?><div class="plugin_settings">
    <div class="position_settings settings_type">
        <div class="input-field col s12">
            <select class="contentPosition browser-default">
                <option value="" disabled selected><?php echo $_smarty_tpl->tpl_vars['editor']->value['position']['title'];?>
</option>
                <?php  $_smarty_tpl->tpl_vars['positions'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['positions']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['editor']->value['position']['items']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['positions']->key => $_smarty_tpl->tpl_vars['positions']->value) {
$_smarty_tpl->tpl_vars['positions']->_loop = true;
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['positions']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['positions']->value;?>
</option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="size_settings settings_type">
        <div class="input-field col s12">
            <select class="contentSize browser-default">
                <option value="" disabled selected><?php echo $_smarty_tpl->tpl_vars['editor']->value['size']['title'];?>
</option>
                <?php  $_smarty_tpl->tpl_vars['size'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['size']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['editor']->value['size']['items']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['size']->key => $_smarty_tpl->tpl_vars['size']->value) {
$_smarty_tpl->tpl_vars['size']->_loop = true;
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['size']->value['value'];?>
"><?php echo $_smarty_tpl->tpl_vars['size']->value['title'];?>
</option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="delete_settings settings_type">
        <a class="contentDelete btn-floating btn-large waves-effect waves-light red">
            <i class="material-icons fa fa-trash"></i>
        </a>
    </div>
    <div class="save_settings settings_type">
        <a class="contentSave btn-floating btn-large waves-effect waves-light">
            <i class="material-icons fa fa-save"></i>
        </a>
    </div>
</div><?php }} ?>
