<?php
class plugin_passengers extends plugin
{
    public function getPassengers($passengersType)
    {
        $this->data['settings'] = $this->getPluginSettings($passengersType);
        return smartyTpl::loadSmarty($this->title,$this->data,$passengersType,$this->method);
    }
}