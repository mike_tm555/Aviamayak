<?php
class smartyTpl
{
    private static $i_smarty;

    public static function getTemplateType($template)
    {
        if(strpos($template, MODULE) !== false) {
            $templatePath = MODULES_PATH;
        } else {
            $templatePath = PLUGINS_PATH;
        }
        return $templatePath;
    }

    public static function loadSmarty($templateFolder,$data = null,$template='index',$method = 'display')
    {
        if (!isset(self::$i_smarty)) {
            $smarty = new Smarty();
            $smarty->setCompileDir(CACHE_DIR);
            $smarty->setCacheDir(CACHE_DIR);
            self::$i_smarty = $smarty;
        }
        if($data != null) {
            foreach($data as $key =>$value) {
                self::$i_smarty->assign($key,$value);
            }
        }
        return self::$i_smarty->$method(self::getTemplateType($templateFolder).$templateFolder.TEMPLATES.$template.TPL);
    }
}