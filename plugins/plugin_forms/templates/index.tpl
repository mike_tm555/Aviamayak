<div class="{$form.blockClass}">
    <form  class="form {$form.class}"
           id="{$form.name}"
            {if $form.action != false}
                action="{$form.action}"
            {/if}
           method="{$form.method}"
           data-name="{$form.name}"
            {if $form.ajax != false}
                data-ajax="{$form.ajax}"
            {/if}
           data-language="{$form.language}">
        {foreach from=$form.steps item=step}
            <div class="form_step"
                 id="step_{$form.name}_{$step.stepID}"
                 data-controller="{$step.stepController}"
                 data-action="{$step.stepAction}">
                {foreach from=$step.elements item=element}
                    <div class="form-group form-group-label" {if $element.htmlType == 'input' && $element.type == "hidden"}style="display:none;"{/if}>
                        <div class="row">
                            <div class="{$element.size}">
                                {if $element.icon != false}
                                    <i class="{$element.icon}"></i>
                                {/if}
                                {if $element.htmlType == "input"}
                                    {if $element.type == "submit" || $element.type == "button" }
                                        <button class="{$element.dataType} {$element.className}"
                                               type="{$element.type}"
                                               data-step="step_{$form.name}_{$step.stepID}">
                                            {$element.values[0]}
                                            </button>
                                    {elseif $element.type == "hidden"}
                                        <input class="{$element.dataType} {$element.className}"
                                               name="{$element.name}"
                                               id="{$element.name}"
                                               type="{$element.type}"
                                               data-step="step_{$form.name}_{$step.stepID}"
                                               data-rules="{$element.rules}"
                                               data-display="{$element.display}"
                                               data-depends="{$element.depends}"
                                               value="{$element.values[0]}"
                                                {if $element.placeholder != false}
                                                    placeholder="{$element.placeholder}"
                                                {/if}
                                               data-dependence="{$element.dependence}"/>

                                    {elseif $element.type == "radio" || $element.type == "checkbox"}
                                        <span class="group_inputs_title">{$element.title}</span>
                                        {if $element.group != false}
                                            <div class="group_elements">
                                                {foreach from=$element.group_elements item=group_element}
                                                    <div class="{$group_element.size}">
                                                        <label for="{$group_element.id}">
                                                            {$group_element.label}
                                                            <input class="{$group_element.dataType} {$group_element.className}"
                                                                   name="{$group_element.name}"
                                                                   id="{$group_element.id}"
                                                                   type="{$element.type}"
                                                                   value="{$group_element.values[0]}"
                                                                   data-rules="{$group_element.rules}"
                                                                   data-display="{$group_element.display}"
                                                                   data-depends="{$group_element.depends}"
                                                                    {if $group_element.placeholder != false}
                                                                        placeholder="{$group_element.placeholder}"
                                                                    {/if}
                                                                   data-dependence="{$group_element.dependence}"/>
                                                            <span class="circle"></span>
                                                            <span class="circle-check"></span>
                                                        </label>
                                                    </div>
                                                {/foreach}
                                            </div>
                                        {/if}
                                    {else}
                                        <input class="{$element.dataType} {$element.className}"
                                               name="{$element.name}"
                                               id="{$element.name}"
                                               type="{$element.type}"
                                               data-step="step_{$form.name}_{$step.stepID}"
                                               data-rules="{$element.rules}"
                                               data-display="{$element.display}"
                                               data-depends="{$element.depends}"
                                                {if $element.placeholder != false}
                                                    placeholder="{$element.placeholder}"
                                                {/if}
                                               data-dependence="{$element.dependence}"/>
                                    {/if}
                                {elseif $element.htmlType == "select"}
                                    <select class="{$element.dataType} {$element.className}"
                                            name="{$element.name}"
                                            id="{$element.name}"
                                            data-step="step_{$form.name}_{$step.stepID}"
                                            data-rules="{$element.rules}"
                                            data-display="{$element.display}"
                                            data-depends="{$element.depends}"
                                            data-dependence="{$element.dependence}">
                                        {if $element.default != false}
                                            <option value="null">{$element.default}</option>
                                        {/if}
                                        {section name=i loop=$element.values}
                                            <option value="{$element.values[i].data}">{$element.values[i].value}</option>
                                        {/section}
                                    </select>
                                {elseif $element.htmlType == "textarea"}
                                    <textarea class="{$element.dataType} {$element.className}"
                                              name="{$element.name}"
                                              id="{$element.name}"
                                              data-step="step_{$form.name}_{$step.stepID}"
                                              data-rules="{$element.rules}"
                                              data-display="{$element.display}"
                                              data-depends="{$element.depends}"
                                            {if $element.placeholder != false}
                                                placeholder="{$element.placeholder}"
                                            {/if}
                                              data-dependence="{$element.dependence}"></textarea>
                                {/if}
                                {if $element.label != false}
                                    <label class="floating-label" for="{$element.name}">{$element.label}</label>
                                {/if}
                            </div>
                        </div>
                    </div>
                {/foreach}
            </div>
        {/foreach}
    </form>
</div>