<?php
class module_comments extends module
{
    protected function getData()
    {
        $this->data['comments'] = $this->getModuleSettings();
    }
}