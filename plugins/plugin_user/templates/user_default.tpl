<div class="user_default">
    <div class="user_title_block">
        <span class="user_title">{$user.settings.unLogged.title}</span>
        <span class="user_text">{$user.settings.unLogged.text}</span>
    </div>

    <div class="user_forms_menu">
        <div class="user_forms_tabs">
            <ul class="tabs">
                {foreach from = $user.settings.unLogged.forms item=tab}
                    <li class="tab col s3">
                        <a href="#{$tab.action}">{$tab.title}</a>
                    </li>
                {/foreach}
            </ul>
        </div>
    </div>

    <div class="user_form_block">
        {foreach from = $user.settings.unLogged.forms item=form}
            <div class="user_form" id="{$form.action}">
                {if $form.text}
                    <span class="user_form_text">{$form.text}</span>
                {/if}
                {${$form.formName}}
            </div>
        {/foreach}
    </div>
</div>