<div class="module_partners">
    <div class="partners_slider_block">
        <span id="partners_button_left" class="slide_button button_left"><i class="fa fa-angle-left"></i></span>
        <span id="partners_button_right" class="slide_button button_right"><i class="fa fa-angle-right"></i></span>
        <div class="partners_slider">
            {foreach from=$partners.partners item=partner}
                <div class="slider_item lazyOwl" {if $partner.image}data-src="{$partner.image}"{/if}>
                    {if $partner.banner}
                        {$partner.banner}
                    {else}
                        {if $partner.title}
                            <span class="partner_title">{$partner.title}</span>
                        {/if}
                        {if $partner.icon}
                            <span class="{if !$partner.title}only_icon{else}and_icon{/if} partner_icon">
                                <i class="{$partner.icon}"></i>
                            </span>
                        {/if}
                    {/if}
                </div>
            {/foreach}
        </div>
    </div>
</div>