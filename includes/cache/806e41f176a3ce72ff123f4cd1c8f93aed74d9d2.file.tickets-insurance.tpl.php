<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-12-30 22:50:53
         compiled from "/vagrant/web/Aviamayak/plugins/plugin_searches/templates/external/tickets-insurance.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1592785228568363b5dfeea4-98872183%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '806e41f176a3ce72ff123f4cd1c8f93aed74d9d2' => 
    array (
      0 => '/vagrant/web/Aviamayak/plugins/plugin_searches/templates/external/tickets-insurance.tpl',
      1 => 1451515848,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1592785228568363b5dfeea4-98872183',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_568363b5ec8671_90994995',
  'variables' => 
  array (
    'destinations' => 0,
    'settings' => 0,
    'passengers' => 0,
    'passenger' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_568363b5ec8671_90994995')) {function content_568363b5ec8671_90994995($_smarty_tpl) {?><div class="inputs_block" id="tickets-insurance">
    <div class="input_small input_block">
        <div class="input-field col s6">
            <input id="destinationTo" data-data='<?php echo $_smarty_tpl->tpl_vars['destinations']->value;?>
' type="text" class="input_text input autoComplete">
            <label for="destinationTo"><?php echo $_smarty_tpl->tpl_vars['settings']->value['searchElements']['destinationTo']['title'];?>
</label>
            <div class="output" id="destinationTo_output">
            </div>
        </div>
    </div>
    <div class="input_small input_block">
        <div id="dateFrom_block" class="input-field col s6 right_only">
            <input id="dateFrom" type="text" readonly class="input_text input date">
            <label for="dateFrom"><?php echo $_smarty_tpl->tpl_vars['settings']->value['searchElements']['dateFrom']['title'];?>
</label>
        </div>
    </div>
    <div class="input_small input_block">
        <div id="dateTo_block" class="input-field col s6 left_only">
            <input id="dateTo" type="text" readonly class="input_text input date">
            <label for="dateTo"><?php echo $_smarty_tpl->tpl_vars['settings']->value['searchElements']['dateTo']['title'];?>
</label>
        </div>
    </div>
    <div class="input_small input_block">
        <div class="input-field col s6">
            <input id="destinationTo"
                   type="text"
                   data-activates='dropdownPassengers'
                   value="<?php echo $_smarty_tpl->tpl_vars['settings']->value['searchElements']['passengers']['default'];?>
"
                   class="input_text input dropdown-button">
        </div>
        <ul id='dropdownPassengers' class='dropdown-content'>
            <?php  $_smarty_tpl->tpl_vars['passenger'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['passenger']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['passengers']->value['passengers']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['passenger']->key => $_smarty_tpl->tpl_vars['passenger']->value) {
$_smarty_tpl->tpl_vars['passenger']->_loop = true;
?>
                <li>
                    <div class="passenger_block">
                        <span class="increment-label"><?php echo $_smarty_tpl->tpl_vars['passenger']->value['value'];?>
</span>
                        <div class="increment-block">
                            <div id="dec" class="dec num-button"><i class="fa fa-minus"></i></div>
                            <input type="number"
                                   min="<?php echo $_smarty_tpl->tpl_vars['passenger']->value['min'];?>
"
                                   max="<?php echo $_smarty_tpl->tpl_vars['passenger']->value['max'];?>
"
                                   id="<?php echo $_smarty_tpl->tpl_vars['passenger']->value['id'];?>
"
                                   class="increment-input"
                                   value="<?php echo $_smarty_tpl->tpl_vars['passenger']->value['count'];?>
">
                            <div id="inc" class="inc num-button"><i class="fa fa-plus"></i></div>
                        </div>
                    </div>
                </li>
                <li class="divider"></li>
                <li>
                    <input type="checkbox" id="multipleInsurance" />
                    <label for="multipleInsurance"><?php echo $_smarty_tpl->tpl_vars['settings']->value['searchElements']['multipleInsurance']['title'];?>
</label>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>
<div class="button_block">
    <div class="button_inner_block">
        <button class="<?php echo $_smarty_tpl->tpl_vars['settings']->value['searchElements']['submit']['class'];?>
">
            <?php echo $_smarty_tpl->tpl_vars['settings']->value['searchElements']['submit']['title'];?>

        </button>
    </div>
</div>
<?php }} ?>
