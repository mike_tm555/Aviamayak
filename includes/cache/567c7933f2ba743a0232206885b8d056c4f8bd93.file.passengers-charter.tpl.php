<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-01-17 20:30:19
         compiled from "/vagrant/web/Aviamayak/plugins/plugin_passengers/templates/passengers-charter.tpl" */ ?>
<?php /*%%SmartyHeaderCode:116845965456998bc026f272-37196433%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '567c7933f2ba743a0232206885b8d056c4f8bd93' => 
    array (
      0 => '/vagrant/web/Aviamayak/plugins/plugin_passengers/templates/passengers-charter.tpl',
      1 => 1453062617,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '116845965456998bc026f272-37196433',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56998bc02c3157_90873781',
  'variables' => 
  array (
    'settings' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56998bc02c3157_90873781')) {function content_56998bc02c3157_90873781($_smarty_tpl) {?><div class="passengers-block" id="passengers-charter">
    <i class="pointer"></i>
    <div class="passengers-block-passengers">
        <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['settings']->value['passengers']['items']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
            <div class="passengers-item">
                <span class="passengers-title"><?php echo $_smarty_tpl->tpl_vars['item']->value['title'];?>
</span>
                <div class="passengers-input-block">
                <span class="passengers-minus" onclick="AVM.TravelReservation.Passengers.countChange('minus','<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
')">
                    <i class="fa fa-minus"></i>
                </span>

                    <input id="<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
"
                           type="number"
                           readonly="readonly"
                           class="passengers-input"
                           min="<?php echo $_smarty_tpl->tpl_vars['item']->value['min'];?>
"
                           max="<?php echo $_smarty_tpl->tpl_vars['item']->value['max'];?>
"
                           value="<?php echo $_smarty_tpl->tpl_vars['item']->value['default'];?>
"/>

                <span class="passengers-plus" onclick="AVM.TravelReservation.Passengers.countChange('plus','<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
')">
                    <i class="fa fa-plus"></i>
                </span>
                </div>
            </div>
        <?php } ?>
    </div>
    <div class="passenger-block-classes">
        <input type="checkbox"
               class="filled-in"
               id="passenger-class-type"
               data-checked-data="<?php echo $_smarty_tpl->tpl_vars['settings']->value['classes']['checked']['data'];?>
"
               data-unchecked-data="<?php echo $_smarty_tpl->tpl_vars['settings']->value['classes']['unchecked']['data'];?>
"
               data-checked-title="<?php echo $_smarty_tpl->tpl_vars['settings']->value['classes']['checked']['title'];?>
"
               data-unchecked-title="<?php echo $_smarty_tpl->tpl_vars['settings']->value['classes']['unchecked']['title'];?>
"/>
        <label for="passenger-class-type" class="passengers-classes-label">
            <?php echo $_smarty_tpl->tpl_vars['settings']->value['classes']['default'];?>

        </label>
    </div>
</div><?php }} ?>
