<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-12-22 05:48:27
         compiled from "/vagrant/web/Aviamayak/modules/module_slide/templates/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:13596674745678e42b3e1e10-04392655%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f3fcf7df4c0a4b773611ad5324bca13a21f66ba8' => 
    array (
      0 => '/vagrant/web/Aviamayak/modules/module_slide/templates/index.tpl',
      1 => 1450225199,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13596674745678e42b3e1e10-04392655',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'slider' => 0,
    'sliderItem' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5678e42b61a8f8_05742633',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5678e42b61a8f8_05742633')) {function content_5678e42b61a8f8_05742633($_smarty_tpl) {?><div class="module_slide">
    <div class="slider fullscreen">
        <span class="slide-button slide-prev">
            <i class="fa fa-angle-left"></i>
        </span>
        <span class="slide-button slide-next">
            <i class="fa fa-angle-right"></i>
        </span>
        <div class="slider-top">
        </div>
        <ul class="slides">
            <?php  $_smarty_tpl->tpl_vars['sliderItem'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['sliderItem']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['slider']->value['slider']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['sliderItem']->key => $_smarty_tpl->tpl_vars['sliderItem']->value) {
$_smarty_tpl->tpl_vars['sliderItem']->_loop = true;
?>
                <li>
                    <img src="<?php echo $_smarty_tpl->tpl_vars['sliderItem']->value['image'];?>
">
                    <div class="<?php echo $_smarty_tpl->tpl_vars['sliderItem']->value['captionClass'];?>
">
                        <h3><?php echo $_smarty_tpl->tpl_vars['sliderItem']->value['title'];?>
</h3>
                        <h5 class="light grey-text text-lighten-3"><?php echo $_smarty_tpl->tpl_vars['sliderItem']->value['subTitle'];?>
</h5>
                    </div>
                </li>
            <?php } ?>
        </ul>
        <div class="slider-bottom">
        </div>
    </div>
</div><?php }} ?>
