<?php
class module_navigation extends module
{
    protected function getData()
    {
        $this->data['navMenu'] = $this->getModuleSettings();
        $this->data['logo'] = plugin_logo::getInstance()->getPlugin();
        $this->data['search'] = plugin_search::getInstance()->getPlugin();
        $this->data['order'] = plugin_order::getInstance()->getPlugin();
        $this->data['user'] = plugin_user::getInstance()->getPlugin();
    }
}