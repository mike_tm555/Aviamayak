<div class="module_travelReservation">
    <div class="search_block">

        {*tickets search view*}
        <div class="inputs_block">
            {foreach from=$settings.searches item=search}
                <div class="{$search.blockClass} input_block">
                    <div class="input-field">
                        <input id="{$search.id}"
                               type="{$search.type}"
                                {if $search.activity}
                                    data-activity="{$search.activity}"
                                {/if}
                                {if $search.data}
                                    data-destinations='{${$search.data}}'
                                {/if}
                                {if $search.readOnly}
                                    readonly="readonly"
                                {/if}
                               class="{$search.class}">
                        <label for="{$search.id}">
                            {$search.title}
                        </label>
                <span class="search_icon">
                    <i class="{$search.icon}"></i>
                </span>
                        {if $search.inner}
                            {${$search.inner}}
                        {/if}
                    </div>
                </div>
            {/foreach}
        </div>

        {*Search trigger button*}
        <div class="button_block">
            <div class="button_inner_block waves-effect">
                <button class="{$settings.submit.class} waves-light">
                    {$settings.submit.title}
                </button>
            </div>
        </div>

        {*Search values form for serialize *}
        <form class="search-form" id="{$settings.searchForm.id}">
            {foreach from=$settings.searchForm.inputs item=input}
                <input type="hidden" name="{$input}">
            {/foreach}
        </form>

    </div>
</div>