<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-12-31 02:24:43
         compiled from "/vagrant/web/Aviamayak/plugins/plugin_searches/templates/internal/tickets-charter.tpl" */ ?>
<?php /*%%SmartyHeaderCode:630975580567e0434e21e05-61687745%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2ea9374a753aceea17ad2b8d49fb62c6ac97044d' => 
    array (
      0 => '/vagrant/web/Aviamayak/plugins/plugin_searches/templates/internal/tickets-charter.tpl',
      1 => 1451528679,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '630975580567e0434e21e05-61687745',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_567e0434e98143_17053352',
  'variables' => 
  array (
    'settings' => 0,
    'destinations' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_567e0434e98143_17053352')) {function content_567e0434e98143_17053352($_smarty_tpl) {?><div class="inputs_block">
    <div class="input_small input_block">
        <div class="input-field">
            <input id="<?php echo $_smarty_tpl->tpl_vars['settings']->value['searches']['destinationFrom']['id'];?>
"
                   data-data='<?php echo $_smarty_tpl->tpl_vars['destinations']->value;?>
'
                   type="text"
                   class="input_text input">
            <label for="<?php echo $_smarty_tpl->tpl_vars['settings']->value['searches']['destinationFrom']['id'];?>
">
                <?php echo $_smarty_tpl->tpl_vars['settings']->value['searches']['destinationFrom']['title'];?>

            </label>
        </div>
    </div>
    <div class="input_small input_block">
        <div class="input-field">
            <input id="<?php echo $_smarty_tpl->tpl_vars['settings']->value['searches']['destinationTo']['id'];?>
"
                   data-data='<?php echo $_smarty_tpl->tpl_vars['destinations']->value;?>
'
                   type="text"
                   class="input_text input">
            <label for="<?php echo $_smarty_tpl->tpl_vars['settings']->value['searches']['destinationTo']['id'];?>
">
                <?php echo $_smarty_tpl->tpl_vars['settings']->value['searches']['destinationTo']['title'];?>

            </label>
        </div>
    </div>
    <div class="input_smallest input_block">
        <div class="input-field">
            <input id="<?php echo $_smarty_tpl->tpl_vars['settings']->value['searches']['dateFrom']['id'];?>
"
                   type="text"
                   readonly="readonly"
                   class="input_text input">
            <label for="<?php echo $_smarty_tpl->tpl_vars['settings']->value['searches']['dateFrom']['id'];?>
">
                <?php echo $_smarty_tpl->tpl_vars['settings']->value['searches']['dateFrom']['title'];?>

            </label>
        </div>
    </div>
    <div class="input_smallest input_block">
        <div class="input-field">
            <input id="<?php echo $_smarty_tpl->tpl_vars['settings']->value['searches']['dateTo']['id'];?>
"
                   type="text"
                   readonly="readonly"
                   class="input_text input">
            <label for="<?php echo $_smarty_tpl->tpl_vars['settings']->value['searches']['dateTo']['id'];?>
">
                <?php echo $_smarty_tpl->tpl_vars['settings']->value['searches']['dateTo']['title'];?>

            </label>
        </div>
    </div>
    <div class="input_small input_block">
        <div class="input-field">
            <input id="<?php echo $_smarty_tpl->tpl_vars['settings']->value['searches']['passengersAndClasses']['id'];?>
"
                   type="text"
                   data-activates='dropdownPassengers'
                   class="input_text input">
            <label for="<?php echo $_smarty_tpl->tpl_vars['settings']->value['searches']['passengersAndClasses']['id'];?>
">
                <?php echo $_smarty_tpl->tpl_vars['settings']->value['searches']['passengersAndClasses']['title'];?>

            </label>
        </div>
    </div>
</div>
<div class="button_block">
    <div class="button_inner_block">
        <button class="<?php echo $_smarty_tpl->tpl_vars['settings']->value['searches']['submit']['class'];?>
">
            <?php echo $_smarty_tpl->tpl_vars['settings']->value['searches']['submit']['title'];?>

        </button>
    </div>
</div>
<?php }} ?>
