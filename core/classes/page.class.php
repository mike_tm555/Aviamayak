<?php
class page
{
    public
        $cache = true,
        $redact = false,
        $component,
        $page,
        $seoLink,
        $isMobile,
        $isAjax;

    protected static $instance;

    public static function getInstance()
    {
        if (self::$instance === null) {

            self::$instance = new self;
        }
        return self::$instance;
    }

    protected function __construct()
    {
        session_start();
        if(isset($_GET['page']) && $_GET['page'] != ''){
            $this->seoLink = explode("/", $this->formatString($_GET['page']));
            foreach($this->seoLink as $key => $value) {
                $this->seoLink[$key] = ($value == '' || $value == null || $value == false) ? false : $value;
            }
            $this->component = $this->seoLink[0];
            $this->page = (isset($this->seoLink[1]) && $this->seoLink[1] != false) ? $this->seoLink[1] : MAIN;
        } else {
            $this->seoLink = false;
            $this->component = MAIN;
            $this->page = MAIN;
        }

        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == XML_REQUEST) {
            $this->isAjax = true;
        }
    }

    protected function formatString($string)
    {
        $string = trim($string);
        $string = stripslashes($string);
        $string = htmlspecialchars($string);
        $string = preg_replace("/[^a-zA-Z0-9-\/\s]/", "", $string);
        return $string;
    }

    public function not_found()
    {

    }

    public function printPage()
    {
        $component = new component($this->redact,$this->component,$this->page);
        $component->printComponent();
    }
}