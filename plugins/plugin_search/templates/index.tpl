<div id="elastic-search" class="plugin_search">
        <span class="search-button"><i class="fa fa-search"></i></span>
        <input type="search" placeholder="{$plugin.title}" autofocus>
        <span class="close-button" onclick="closeElasticSearch()"><i class="fa fa-times-circle-o"></i></span>
</div>