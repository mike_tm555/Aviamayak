<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-12-22 16:51:58
         compiled from "/vagrant/web/Aviamayak/modules/module_popular/templates/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12687501255678e42b65e427-49308365%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4655e16efca1ee833c2afaf206302f1433af1c53' => 
    array (
      0 => '/vagrant/web/Aviamayak/modules/module_popular/templates/index.tpl',
      1 => 1450803115,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12687501255678e42b65e427-49308365',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5678e42b778485_30055317',
  'variables' => 
  array (
    'destinations' => 0,
    'menuItem' => 0,
    'tab' => 0,
    'destination' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5678e42b778485_30055317')) {function content_5678e42b778485_30055317($_smarty_tpl) {?><div class="module_popular">
    <div class="popular_block first">
        <div class="popular_header">
            <div class="popular_block_title">
                <?php echo $_smarty_tpl->tpl_vars['destinations']->value['title']['title'];?>

            </div>
            <div class="col s12 destinations-menu">
                <ul class="tabs">
                    <?php  $_smarty_tpl->tpl_vars['menuItem'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['menuItem']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['destinations']->value['destinationsMenu']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['menuItem']->key => $_smarty_tpl->tpl_vars['menuItem']->value) {
$_smarty_tpl->tpl_vars['menuItem']->_loop = true;
?>
                        <li class="tab col s3">
                            <a class="<?php echo $_smarty_tpl->tpl_vars['menuItem']->value['class'];?>
" href="<?php echo $_smarty_tpl->tpl_vars['menuItem']->value['seoLink'];?>
">
                            <span class="tabTitle">
                                <?php echo $_smarty_tpl->tpl_vars['menuItem']->value['title'];?>

                                </span>
                            <span class="tab_inner btn-floating btn-large waves-effect waves-light green">
                    <i class="<?php echo $_smarty_tpl->tpl_vars['menuItem']->value['icon'];?>
"></i>
                        </span>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="popular_block second">
        <div class="popular_content">
            <?php  $_smarty_tpl->tpl_vars['tab'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['tab']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['destinations']->value['destinationTabs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['tab']->key => $_smarty_tpl->tpl_vars['tab']->value) {
$_smarty_tpl->tpl_vars['tab']->_loop = true;
?>
                <div id="<?php echo $_smarty_tpl->tpl_vars['tab']->value['id'];?>
" class="tabCol">
                    <?php  $_smarty_tpl->tpl_vars['destination'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['destination']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tab']->value['destinations']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['destination']->key => $_smarty_tpl->tpl_vars['destination']->value) {
$_smarty_tpl->tpl_vars['destination']->_loop = true;
?>
                        <div class="popular_item waves-effect waves-light <?php echo $_smarty_tpl->tpl_vars['destination']->value['class'];?>
">
                            <div class="popular_img" style="background-image: url('<?php echo $_smarty_tpl->tpl_vars['destination']->value['image'];?>
');" title="<?php echo $_smarty_tpl->tpl_vars['destination']->value['title'];?>
"></div>
                            <span class="popular_title"><?php echo $_smarty_tpl->tpl_vars['destination']->value['title'];?>
</span>
                            <span class="popular_country"><?php echo $_smarty_tpl->tpl_vars['destination']->value['country'];?>
</span>
                            <?php if ($_smarty_tpl->tpl_vars['destination']->value['subTitle']!=false) {?>
                                <span class="popular_subTitle"><?php echo $_smarty_tpl->tpl_vars['destination']->value['subTitle'];?>
</span>
                            <?php }?>
                            <span class="popular_season"><?php echo $_smarty_tpl->tpl_vars['destination']->value['season'];?>
</span>
                        </div>
                    <?php } ?>
                    <a class="waves-effect waves-light btn-large all_destinations">
                        <?php echo $_smarty_tpl->tpl_vars['destinations']->value['button']['title'];?>

                    </a>
                </div>
            <?php } ?>
        </div>
    </div>
</div><?php }} ?>
