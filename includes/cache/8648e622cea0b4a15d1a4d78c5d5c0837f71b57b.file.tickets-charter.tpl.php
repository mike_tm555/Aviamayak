<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-01-17 20:04:57
         compiled from "/vagrant/web/Aviamayak/modules/module_travelReservation/templates/tickets-charter.tpl" */ ?>
<?php /*%%SmartyHeaderCode:138644402656892da34b9705-57130195%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8648e622cea0b4a15d1a4d78c5d5c0837f71b57b' => 
    array (
      0 => '/vagrant/web/Aviamayak/modules/module_travelReservation/templates/tickets-charter.tpl',
      1 => 1453061095,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '138644402656892da34b9705-57130195',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56892da357d354_29641914',
  'variables' => 
  array (
    'settings' => 0,
    'search' => 0,
    '($_smarty_tpl->tpl_vars[\'search\']->value[\'data\'])' => 0,
    '($_smarty_tpl->tpl_vars[\'search\']->value[\'inner\'])' => 0,
    'input' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56892da357d354_29641914')) {function content_56892da357d354_29641914($_smarty_tpl) {?><div class="module_travelReservation">
    <div class="search_block">

        
        <div class="inputs_block">
            <?php  $_smarty_tpl->tpl_vars['search'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['search']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['settings']->value['searches']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['search']->key => $_smarty_tpl->tpl_vars['search']->value) {
$_smarty_tpl->tpl_vars['search']->_loop = true;
?>
                <div class="<?php echo $_smarty_tpl->tpl_vars['search']->value['blockClass'];?>
 input_block">
                    <div class="input-field">
                        <input id="<?php echo $_smarty_tpl->tpl_vars['search']->value['id'];?>
"
                               type="<?php echo $_smarty_tpl->tpl_vars['search']->value['type'];?>
"
                                <?php if ($_smarty_tpl->tpl_vars['search']->value['activity']) {?>
                                    data-activity="<?php echo $_smarty_tpl->tpl_vars['search']->value['activity'];?>
"
                                <?php }?>
                                <?php if ($_smarty_tpl->tpl_vars['search']->value['data']) {?>
                                    data-destinations='<?php echo $_smarty_tpl->tpl_vars[($_smarty_tpl->tpl_vars['search']->value['data'])]->value;?>
'
                                <?php }?>
                                <?php if ($_smarty_tpl->tpl_vars['search']->value['readOnly']) {?>
                                    readonly="readonly"
                                <?php }?>
                               class="<?php echo $_smarty_tpl->tpl_vars['search']->value['class'];?>
">
                        <label for="<?php echo $_smarty_tpl->tpl_vars['search']->value['id'];?>
">
                            <?php echo $_smarty_tpl->tpl_vars['search']->value['title'];?>

                        </label>
                <span class="search_icon">
                    <i class="<?php echo $_smarty_tpl->tpl_vars['search']->value['icon'];?>
"></i>
                </span>
                        <?php if ($_smarty_tpl->tpl_vars['search']->value['inner']) {?>
                            <?php echo $_smarty_tpl->tpl_vars[($_smarty_tpl->tpl_vars['search']->value['inner'])]->value;?>

                        <?php }?>
                    </div>
                </div>
            <?php } ?>
        </div>

        
        <div class="button_block">
            <div class="button_inner_block waves-effect">
                <button class="<?php echo $_smarty_tpl->tpl_vars['settings']->value['submit']['class'];?>
 waves-light">
                    <?php echo $_smarty_tpl->tpl_vars['settings']->value['submit']['title'];?>

                </button>
            </div>
        </div>

        
        <form class="search-form" id="<?php echo $_smarty_tpl->tpl_vars['settings']->value['searchForm']['id'];?>
">
            <?php  $_smarty_tpl->tpl_vars['input'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['input']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['settings']->value['searchForm']['inputs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['input']->key => $_smarty_tpl->tpl_vars['input']->value) {
$_smarty_tpl->tpl_vars['input']->_loop = true;
?>
                <input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['input']->value;?>
">
            <?php } ?>
        </form>

    </div>
</div><?php }} ?>
