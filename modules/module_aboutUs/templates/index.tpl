<div class="module_aboutUs">
    <div class="title">
        <span class="title_text">{$aboutUs.title}</span>
        <span class="title_icon btn-floating btn-large waves-effect waves-light transparent"><i class="fa fa-arrow-down"></i></span>
    </div>
    <div class="aboutUs_slider">
        <span id="aboutUs_button_left" class="slide_button button_left"><i class="fa fa-angle-left"></i></span>
        <span id="aboutUs_button_right" class="slide_button button_right"><i class="fa fa-angle-right"></i></span>
        <div class="aboutUs_slider_inner">
        <div class="aboutUs_slider_title">
            <span class="aboutUs_slider_title_block"></span>
            <span class="aboutUs_slider_title_separator"></span>
        </div>
        <div class="aboutUs_slider_block">
            {foreach from=$aboutUs.slider item=sliderItem}
                <div class="aboutUs_slider_item">
                    <div class="slider_icon"><i class="{$sliderItem.icon}"></i></div>
                    <div class="slider_title">{$sliderItem.title}</div>
                    <div class="slider_text">{$sliderItem.text}</div>
                </div>
            {/foreach}
        </div>
    </div>
    </div>
</div>