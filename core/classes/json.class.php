<?php
class json
{
    private static $instance;

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    public function getJsonInternal($jsonAddress)
    {
        return json_decode(file_get_contents(PATH.$jsonAddress),true);
    }
}