<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-01-10 19:13:25
         compiled from "/vagrant/web/Aviamayak/modules/module_footer/templates/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:9051681755678e42c75cee3-94343944%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '56e7bc09de779103dc5cbfd6160ceea53cd8321d' => 
    array (
      0 => '/vagrant/web/Aviamayak/modules/module_footer/templates/index.tpl',
      1 => 1452453204,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '9051681755678e42c75cee3-94343944',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5678e42c78aff5_29223587',
  'variables' => 
  array (
    'footer' => 0,
    'requisite' => 0,
    'contactUsForm' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5678e42c78aff5_29223587')) {function content_5678e42c78aff5_29223587($_smarty_tpl) {?><div class="module_footer">
    <footer class="page-footer">
        <div class="container footer-header">
            <div class="row">
                <div class="col l6 s12 info requisites-block">
                    <h5 class="white-text">
                        <?php echo $_smarty_tpl->tpl_vars['footer']->value['requisites']['title'];?>

                    </h5>
                    <p class="grey-text text-lighten-4">
                        <?php echo $_smarty_tpl->tpl_vars['footer']->value['requisites']['text'];?>

                    </p>
                    <ul class="requisites_list">
                        <?php  $_smarty_tpl->tpl_vars['requisite'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['requisite']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['footer']->value['requisites']['items']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['requisite']->key => $_smarty_tpl->tpl_vars['requisite']->value) {
$_smarty_tpl->tpl_vars['requisite']->_loop = true;
?>
                            <li>
                                <a class="waves-effect waves-light btn-flat requisite">
                                    <i class="<?php echo $_smarty_tpl->tpl_vars['requisite']->value['icon'];?>
"></i>
                                    &nbsp;<?php echo $_smarty_tpl->tpl_vars['requisite']->value['title'];?>

                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="col l6 offset-l2 s12 form-block">
                    <h5 class="white-text">
                        <?php echo $_smarty_tpl->tpl_vars['footer']->value['formTitle'];?>

                    </h5>
                    <div class="footer-form">
                    <?php echo $_smarty_tpl->tpl_vars['contactUsForm']->value;?>

                    </div>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                <?php echo $_smarty_tpl->tpl_vars['footer']->value['copyRight'];?>

                <a class="site-map text-lighten-4 waves-effect waves-light right" href="<?php echo $_smarty_tpl->tpl_vars['footer']->value['siteMap']['soLink'];?>
">
                    <i class="<?php echo $_smarty_tpl->tpl_vars['footer']->value['siteMap']['icon'];?>
"></i>
                    <?php echo $_smarty_tpl->tpl_vars['footer']->value['siteMap']['title'];?>

                </a>
            </div>
        </div>
    </footer>
</div><?php }} ?>
