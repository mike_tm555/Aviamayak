<div class="module_travelReservation">
    <div class="search_block">
        <div class="inputs_block">
            {foreach from=$settings.searches item=search}
                <div class="{$search.blockClass} input_block">
                    <div class="input-field">
                        <input id="{$search.id}"
                               type="{$search.type}"
                                {if $search.activity}
                                    data-activity="{$search.activity}"
                                {/if}
                                {if $search.data}
                                    data-data="{$search.data}"
                                {/if}
                               class="{$search.class}">
                        <label for="{$search.id}">
                            {$search.title}
                        </label>
                <span class="search_icon">
                    <i class="{$search.icon}"></i>
                </span>
                        {if $search.inner}
                            {$additionsalElements.{$inner}}
                        {/if}
                    </div>
                </div>
            {/foreach}
        </div>
        <div class="button_block">
            <div class="button_inner_block">
                <button class="{$settings.submit.class}">
                    {$settings.submit.title}
                </button>
            </div>
        </div>
    </div>
</div>
