AVM.TravelReservation = {
    setActivity: function() {
        $('.input').each(function () {

            var scrollTopPosition = $(this).offset().top - 120;

            $(this).click(function () {
                $('html,body').animate({"scrollTop": scrollTopPosition}, 500)
            });

            var activityType = $(this).attr('data-activity');

            switch (activityType) {

                case 'datePickerFrom':
                    $(this).datepicker({
                        minDate: new Date(),
                        position: "bottom left"
                    });
                    break;

                case 'datePickerTo':
                    $(this).datepicker({
                        minDate: new Date(),
                        position: "bottom right"
                    });
                    break;

                case 'datePickerRange':
                    $(this).datepicker({
                        minDate: new Date(),
                        range: true
                    });
                    break;

                case 'autoCompleteFrom':
                    var dataFrom = $(this).attr("data-destinations");
                    dataFrom = JSON.parse(dataFrom);
                    $(this).autocomplete({
                        lookup: dataFrom,
                        appendTo: $(this).parent(),
                        forceFixPosition: true,
                        orientation: "auto"
                    });
                    break;

                case 'autoCompleteTo':
                    var dataTo = $(this).attr("data-destinations");
                    dataTo = JSON.parse(dataTo);
                    $(this).autocomplete({
                        lookup: dataTo,
                        appendTo: $(this).parent(),
                        forceFixPosition: true,
                        orientation: "auto"
                    });
                    break;

                case 'passengers':
                    AVM.TravelReservation.Passengers.picker($(this));
                    break;
            }
        });
    }
};

$(document).ready(function() {
    AVM.TravelReservation.setActivity();
});