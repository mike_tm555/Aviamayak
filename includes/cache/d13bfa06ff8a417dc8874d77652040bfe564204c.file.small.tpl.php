<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-11-24 04:28:59
         compiled from "modules/module_newsSlider/templates/small.tpl" */ ?>
<?php /*%%SmartyHeaderCode:4908763735653e78ba30169-60347098%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd13bfa06ff8a417dc8874d77652040bfe564204c' => 
    array (
      0 => 'modules/module_newsSlider/templates/small.tpl',
      1 => 1448165646,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4908763735653e78ba30169-60347098',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'news' => 0,
    'this_index' => 0,
    'news_count' => 0,
    'next_index' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5653e78bbd0833_95053584',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5653e78bbd0833_95053584')) {function content_5653e78bbd0833_95053584($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_truncate')) include '/vagrant/web/Aviamayak/includes/smarty/libs/plugins/modifier.truncate.php';
?><div class="module_newsSlider">
<div class="main_wrapper">
    <div class="this_news" style="background-image: url(<?php echo $_smarty_tpl->tpl_vars['news']->value[0]['data']['image'];?>
)">
        <div class="this_news_description">
            <span class="news_title"><?php echo $_smarty_tpl->tpl_vars['news']->value[0]['title'];?>
</span>
            <span class="news_text"><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['news']->value[0]['text'],100,"...",true);?>
</span>
        </div>
    </div>
    <div class="next_news" data-index="1">
        <div class="next_news_description">
            <span class="news_title"><?php echo $_smarty_tpl->tpl_vars['news']->value[1]['title'];?>
</span>
            <span class="news_text"><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['news']->value[1]['text'],100,"...",true);?>
</span>
        </div>
        <div class="next_news_thumb" style="background-image: url(<?php echo $_smarty_tpl->tpl_vars['news']->value[1]['data']['image'];?>
)"></div>
    </div>
</div>


    <div class="slider_data">
        <?php $_smarty_tpl->tpl_vars["news_count"] = new Smarty_variable(count($_smarty_tpl->tpl_vars['news']->value)-1, null, 0);?>
        <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['news']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
            <?php $_smarty_tpl->tpl_vars["this_index"] = new Smarty_variable($_smarty_tpl->getVariable('smarty')->value['section']['i']['index'], null, 0);?>
            <?php if ($_smarty_tpl->tpl_vars['this_index']->value+1>$_smarty_tpl->tpl_vars['news_count']->value) {?>
                <?php $_smarty_tpl->tpl_vars["next_index"] = new Smarty_variable(0, null, 0);?>
            <?php } else { ?>
                <?php $_smarty_tpl->tpl_vars["next_index"] = new Smarty_variable($_smarty_tpl->getVariable('smarty')->value['section']['i']['index']+1, null, 0);?>
            <?php }?>
            <div class="slider_item" style="display: none"
                data-index = "<?php echo $_smarty_tpl->tpl_vars['this_index']->value;?>
"
                data-next = "<?php echo $_smarty_tpl->tpl_vars['next_index']->value;?>
"
                data-image = "<?php echo $_smarty_tpl->tpl_vars['news']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['data']['image'];?>
"
                data-title = "<?php echo $_smarty_tpl->tpl_vars['news']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['title'];?>
"
                data-description = "<?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['news']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['text'],100,"...",true);?>
"
                data-date = "<?php echo $_smarty_tpl->tpl_vars['news']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['data']['date'];?>
"
            >
             </div>
        <?php endfor; endif; ?>

     </div>
</div><?php }} ?>
