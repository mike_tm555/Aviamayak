<?php
class plugin_forms extends plugin
{
    public function createForm($formName)
    {
        $this->data['form'] = $this->getPluginSettings($formName);
        return smartyTpl::loadSmarty($this->title,$this->data,$this->template,$this->method);
    }
}