<div class="module_news">
    {if $page === false}
        <div class="news_slider">
            <span class="news_slider_button news_left_button"><i class="fa fa-angle-left"></i></span>
            <span class="news_slider_button news_right_button"><i class="fa fa-angle-right"></i></span>
            {*<i class="fa fa-calendar"></i> {$news[i].data.date}*}
            <div class="news_slider_block row">
                {section name=i loop=$news}
                    <div class="news_slider_item">
                        <div class="card large">
                            <div class="card-image">
                                <img src="/Aviamayak{$news[i].data.image}">
                            <span class="card-title">
                                {$news[i].title|truncate:30:"...":true}
                            </span>
                            </div>
                            <div class="card-content">
                                <p>{$news[i].text|truncate:200:"...":true}</p>
                            </div>
                            <div class="card-action">
                                <a class="waves-effect waves-light btn">{$settings.newsMore}</a>
                            </div>
                        </div>
                    </div>
                {/section}
            </div>
        </div>
    {else}

    {/if}
</div>