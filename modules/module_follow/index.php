<?php
class module_follow extends  module
{
    protected function getData()
    {
        $this->data['follow'] = $this->getModuleSettings();
        $this->data['followForm'] = plugin_forms::getInstance()->createForm('followForm');
    }
}